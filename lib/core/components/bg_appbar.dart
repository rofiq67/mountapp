import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';

class BgAppbar extends StatelessWidget {
  final String judulAppbar;
  final iconAct;

  const BgAppbar({required this.judulAppbar, required this.iconAct, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.maxFinite,
        height: 186,
        decoration: BoxDecoration(
            color: const Color(0xFFA6E3A7),
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(16),
                bottomRight: Radius.circular(16))),
        child: PreferredSize(
          preferredSize: const Size(
            double.maxFinite,
            148,
          ),
          child: AppBar(
            title: Text(
              judulAppbar,
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontFamily: "Inter",
                fontWeight: FontWeight.w700,
              ),
            ),
            leading: Icon(
              Icons.arrow_back_ios_rounded,
              color: Colors.black,
            ),
            elevation: 0,
            centerTitle: true,
            backgroundColor: const Color(0xFFA6E3A7),
            shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(16),
                    bottomRight: Radius.circular(16))),
            flexibleSpace: Align(
              alignment: Alignment.bottomRight,
              child: Image.asset(
                'assets/images/bg gunung appbar.png',
              ),
            ),
            actions: [
              Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: Icon(
                  iconAct,
                  color: Colors.black,
                ),
              )
            ],
          ),
        ));
  }
}
