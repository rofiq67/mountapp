import 'package:flutter/material.dart';

class CardRewards extends StatelessWidget {
  final String hariReward;
  final int nilaiReward;
  const CardRewards(
      {required this.hariReward, required this.nilaiReward, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 60,
      height: 65,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Colors.grey.withOpacity(0.5), //color of shadow
              spreadRadius: 1, //spread radius
              blurRadius: 4, // blur radius
              offset: const Offset(0, 0))
        ],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            hariReward,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.black,
              fontSize: 8,
              fontFamily: "Inter",
              fontWeight: FontWeight.w700,
            ),
          ),
          Image.asset(
            'assets/icons/Coin_reward.png',
            height: 22,
            width: 21.11,
          ),
          Text(
            "$nilaiReward Poin",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Color(0x7f000000),
              fontSize: 8,
            ),
          ),
        ],
      ),
    );
  }
}
