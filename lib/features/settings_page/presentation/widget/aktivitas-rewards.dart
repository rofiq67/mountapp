import 'package:flutter/material.dart';

class AktivivtasRewards extends StatelessWidget {
  final iconAktvitas;

  final String judulAktivtas;
  final String deskAktivitas;
  final int nilaiAktivitas;
  const AktivivtasRewards(
      {required this.iconAktvitas,
      required this.judulAktivtas,
      required this.deskAktivitas,
      required this.nilaiAktivitas,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      mainAxisSize: MainAxisSize.min,
      children: [
        Icon(
          iconAktvitas,
          size: 30,
        ),
        SizedBox(
          width: 16,
        ),
        SizedBox(
          width: 191,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                judulAktivtas,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 13,
                  fontFamily: "Inter",
                  fontWeight: FontWeight.w700,
                ),
              ),
              Text(
                deskAktivitas,
                style: TextStyle(
                  color: Color(0xb2000000),
                  fontSize: 10,
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          width: 64,
        ),
        Container(
          width: 57,
          height: 22,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: const Color(0xff03ac0e),
          ),
          child: Row(
              // mainAxisSize: MainAxisSize.min,
              // mainAxisAlignment: MainAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  'assets/icons/Coin_reward.png',
                  height: 12,
                  width: 12,
                ),
                Padding(
                  padding: EdgeInsets.only(left: 4.0),
                  child: Text(
                    nilaiAktivitas.toString(),
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 10,
                      fontFamily: "Inter",
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
              ]),
        )
      ],
    );
  }
}
