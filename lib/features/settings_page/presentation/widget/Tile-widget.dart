import 'package:flutter/material.dart';

class TileWidget extends StatelessWidget {
  final String judulTile;
  final String deskTile;
  final String isiTile;
  const TileWidget(
      {required this.judulTile,
      required this.deskTile,
      required this.isiTile,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text(
        judulTile,
        style: TextStyle(
          color: Colors.black,
          fontSize: 15,
          fontFamily: "Inter",
          fontWeight: FontWeight.w700,
        ),
      ),
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
          width: double.maxFinite,
          height: 164,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border(top: BorderSide(color: Colors.black, width: 0.5))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                deskTile,
                style: TextStyle(
                    fontSize: 12,
                    color: Colors.black,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(
                height: 4,
              ),
              Text(
                isiTile,
                style: TextStyle(
                    fontSize: 12,
                    color: Colors.black,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w500),
              ),
            ],
          ),
        )
      ],
    );
  }
}
