import 'package:flutter/material.dart';

class TentangKami extends StatefulWidget {
  const TentangKami({Key? key}) : super(key: key);

  @override
  State<TentangKami> createState() => _TentangKamiState();
}

class _TentangKamiState extends State<TentangKami> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  width: double.maxFinite,
                  height: 271,
                  child: PreferredSize(
                    preferredSize: Size.fromHeight(271),
                    child: AppBar(
                      elevation: 0,
                      backgroundColor: Color(0xff4eee7b),
                      automaticallyImplyLeading: false,
                      leading: Container(
                        margin: const EdgeInsets.only(left: 16),
                        height: 28,
                        width: 28,
                        decoration: BoxDecoration(
                            color: Colors.white, shape: BoxShape.circle),
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 128, right: 128, top: 15),
                  child: ClipRRect(
                    child: Image.asset(
                      "assets/images/logo-font-putih.png",
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 108, left: 16),
                  child: Stack(children: [
                    Container(
                        width: 185.66,
                        height: 186.74,
                        child: Image.asset(
                            "assets/images/ils-orang-with bg-gunung.png")),
                    Padding(
                      padding: const EdgeInsets.only(left: 159),
                      child: Column(
                        children: [
                          SizedBox(
                            width: 208,
                            height: 50,
                            child: Text(
                              "Raih Gunung Terbaru dengan MountApp!",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 15,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ]),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8.0, left: 16, right: 16),
              child: Column(
                children: [
                  Text(
                    "Tentang MountApp",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      fontFamily: "Inter",
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Container(
                      width: 142.11,
                      height: 139.48,
                      child: Image.asset("assets/images/ils-org-hp.png")),
                  SizedBox(
                    height: 8,
                  ),
                  SizedBox(
                    width: double.maxFinite,
                    height: 160,
                    child: Text(
                      "MountApp membantu Anda mendapatkan tiket yang diinginkan dengan mudah. Dengan MountApp, Anda dapat mencari tiket gunung. Anda dapat memilih tiket yang sesuai keinginan Anda berdasarkan kategori, lokasi, harga, dan lainnya. Selain itu, Anda juga dapat memesan tiket kapan saja dan di mana saja. Anda dapat membayar tiket dengan mudah menggunakan berbagai metode pembayaran. MountApp juga memiliki layanan pelanggan yang responsif dan siap membantu Anda setiap saat. Ayo gunakan MountApp dan dapatkan tiket yang diinginkan dengan mudah!",
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 12,
                      ),
                    ),
                  ),
                  // f2
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                      width: 142.11,
                      height: 139.48,
                      child: Image.asset("assets/images/ils-dua-orang.png")),
                  SizedBox(
                    height: 8,
                  ),
                  SizedBox(
                    width: double.maxFinite,
                    height: 160,
                    child: Text(
                      "MountApp adalah solusi pengelolaan tiket ideal untuk bisnis Anda. Ini memungkinkan Anda mengelola semua tiket dan layanan pelanggan dari satu platform. Dengan MountApp, Anda dapat membuat tiket dengan cepat dan mudah, mengirimkan jawaban otomatis, memonitor status tiket, dan melacak statistik pelanggan. Anda dapat memantau dan melacak semua interaksi pelanggan di satu tempat, memberikan layanan pelanggan yang lebih baik dan pengalaman pelanggan yang lebih baik. Dengan MountApp, Anda juga dapat mengelola notifikasi, membuat laporan, mengintegrasikan dengan aplikasi lain, dan banyak lagi.",
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 12,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 64),
              width: double.maxFinite,
              height: 352,
              color: Color(0xff515b60),
              child: Column(
                children: [
                  SizedBox(
                    height: 32,
                  ),
                  Text(
                    "Nikmati Alam dengan Satu Genggaman",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontFamily: "Inter",
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Text(
                    "Unduh MounTapp  GRATIS sekarang juga!",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                  SizedBox(
                    height: 32,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                          width: 134,
                          height: 45,
                          child: Image.asset("assets/images/playstore.png")),
                      // SizedBox(
                      //   width: 8,
                      // ),
                      Container(
                          width: 134,
                          height: 45,
                          child: Image.asset("assets/images/appstore.png")),
                    ],
                  ),
                  SizedBox(
                    height: 78,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        width: 20,
                      ),
                      Icon(
                        Icons.email_rounded,
                        color: Colors.white,
                      ),
                      Icon(
                        Icons.facebook,
                        color: Colors.white,
                      ),
                      Icon(
                        Icons.tiktok_rounded,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Text(
                    "Copyright 2023 MounTapp",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
