// import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mountapp/core/components/bg_appbar.dart';
import 'package:mountapp/features/settings_page/presentation/widget/aktivitas-rewards.dart';
import 'package:mountapp/features/settings_page/presentation/widget/card-rewards.dart';
// import 'package:flutter/src/foundation/key.dart';
// import 'package:flutter/src/widgets/container.dart';
// import 'package:flutter/src/widgets/framework.dart';

class RewardPage extends StatelessWidget {
  const RewardPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          BgAppbar(
            judulAppbar: "Rewards",
            iconAct: null,
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(17, 108, 17, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  width: 366,
                  height: 214,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(16),
                    boxShadow: const [
                      BoxShadow(
                        color: Color(0x3f000000),
                        blurRadius: 4,
                        offset: Offset(0, 0),
                      ),
                    ],
                    color: Colors.white,
                  ),
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            const Text(
                              "Sign in",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 13,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            const SizedBox(
                              width: 120,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const Text(
                                  "Coin Anda",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Color(0xb2000000),
                                    fontSize: 8,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 3.0),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        'assets/icons/Coin_reward.png',
                                        height: 22,
                                        width: 21.11,
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.only(left: 3.0),
                                        child: Text(
                                          "1230 Poin",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 10,
                                            fontFamily: "Inter",
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 18, vertical: 16),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.grey.withOpacity(
                                              0.5), //color of shadow
                                          spreadRadius: 1, //spread radius
                                          blurRadius: 4, // blur radius
                                          offset: const Offset(0, 0))
                                    ],
                                  ),
                                  child: ElevatedButton(
                                    onPressed: (() {
                                      showDialog(
                                          context: context,
                                          builder: ((context) => AlertDialog(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            8)),
                                                content:
                                                    Builder(builder: (context) {
                                                  return Container(
                                                      width: 328,
                                                      height: 399,
                                                      child: Column(
                                                        // mainAxisAlignment:
                                                        //     MainAxisAlignment
                                                        //         .spaceAround,
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    top: 62.0),
                                                            child: Image.asset(
                                                              'assets/icons/Coin_reward.png',
                                                              width: 108.58,
                                                              height: 113.17,
                                                            ),
                                                          ),
                                                          const Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    top: 5.0),
                                                            child: Text(
                                                              "Congratulation",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 20,
                                                                fontFamily:
                                                                    "Inter",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w700,
                                                              ),
                                                            ),
                                                          ),
                                                          const Padding(
                                                            padding:
                                                                EdgeInsets.only(
                                                                    top: 9.0),
                                                            child: SizedBox(
                                                              width: 296,
                                                              child: Text(
                                                                "Selamat anda mendapatkan koin sebesar 50, lakukan aktivitas sebanyak-banyaknya dan dapatkan hal yang menarik",
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                style:
                                                                    TextStyle(
                                                                  color: Color(
                                                                      0xb2000000),
                                                                  fontSize: 12,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    top: 80.0),
                                                            child:
                                                                ElevatedButton(
                                                              onPressed: () {},
                                                              style: ElevatedButton
                                                                  .styleFrom(
                                                                      minimumSize:
                                                                          const Size(
                                                                              240,
                                                                              45),
                                                                      maximumSize:
                                                                          const Size(
                                                                              240,
                                                                              45),
                                                                      backgroundColor:
                                                                          const Color(
                                                                              0xff03ac0e),
                                                                      // shadowColor:
                                                                      //     Colors.grey.withOpacity(0.5),
                                                                      // elevation: 2,
                                                                      shape:
                                                                          RoundedRectangleBorder(
                                                                        borderRadius:
                                                                            BorderRadius.circular(8),
                                                                      )),
                                                              child: const Text(
                                                                  "Done"),
                                                            ),
                                                          )
                                                        ],
                                                      ));
                                                }),
                                              )));
                                    }),
                                    style: ElevatedButton.styleFrom(
                                        minimumSize: const Size(60, 65),
                                        maximumSize: const Size(60, 65),
                                        backgroundColor: Colors.white,
                                        // shadowColor:
                                        //     Colors.grey.withOpacity(0.5),
                                        // elevation: 2,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                        )),
                                    child: Column(
                                      // mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        const Text(
                                          "Senin",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 8,
                                            fontFamily: "Inter",
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        Image.asset(
                                          'assets/icons/Coin_reward.png',
                                          height: 22,
                                          width: 21.11,
                                        ),
                                        const SizedBox(
                                          width: 150,
                                          child: Text(
                                            "1230 Poin",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                              color: Color(0x7f000000),
                                              fontSize: 8,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 16,
                                ),
                                CardRewards(
                                  hariReward: "Selasa",
                                  nilaiReward: 25,
                                ),
                                SizedBox(
                                  width: 16,
                                ),
                                CardRewards(
                                  hariReward: "Rabu",
                                  nilaiReward: 15,
                                ),
                                SizedBox(
                                  width: 16,
                                ),
                                CardRewards(
                                  hariReward: "Kamis",
                                  nilaiReward: 25,
                                ),
                              ],
                            ),
                            SizedBox(height: 2),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  CardRewards(
                                    hariReward: "Jumat",
                                    nilaiReward: 25,
                                  ),
                                  SizedBox(
                                    width: 16,
                                  ),
                                  CardRewards(
                                    hariReward: "Sabtu",
                                    nilaiReward: 25,
                                  ),
                                  SizedBox(
                                    width: 16,
                                  ),
                                  Container(
                                    width: 148,
                                    height: 65,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(8),
                                      color: Colors.white,
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.grey.withOpacity(
                                                0.5), //color of shadow
                                            spreadRadius: 1, //spread radius
                                            blurRadius: 4, // blur radius
                                            offset: const Offset(0, 0))
                                      ],
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: const [
                                            Text(
                                              "Minggu",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 8,
                                                fontFamily: "Inter",
                                                fontWeight: FontWeight.w700,
                                              ),
                                            ),
                                            Text(
                                              "Mystery spree",
                                              textAlign: TextAlign.center,
                                              style: TextStyle(
                                                color: Color(0x7f000000),
                                                fontSize: 8,
                                              ),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            Image.asset(
                                              'assets/icons/Coin_reward.png',
                                              height: 42,
                                              width: 40.30,
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 32,
                ),
                Text(
                  "Aktivitas",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w700,
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                AktivivtasRewards(
                    iconAktvitas: Icons.date_range,
                    judulAktivtas: "Aktivitas selama 3 hari",
                    deskAktivitas: "Klaim coin anda sebesar 10 coin",
                    nilaiAktivitas: 10),
                SizedBox(
                  height: 8,
                ),
                AktivivtasRewards(
                    iconAktvitas: Icons.upload_file_outlined,
                    judulAktivtas: "Share photo pertama di Feed",
                    deskAktivitas: "Klaim coin anda sebesar 15 coin",
                    nilaiAktivitas: 15),
                SizedBox(
                  height: 8,
                ),
                AktivivtasRewards(
                    iconAktvitas: Icons.share_rounded,
                    judulAktivtas: "Bagikan aktivitas",
                    deskAktivitas: "Klaim coin anda sebesar 5 coin",
                    nilaiAktivitas: 5),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
