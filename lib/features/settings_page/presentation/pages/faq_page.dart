import 'package:flutter/material.dart';
import 'package:mountapp/features/settings_page/presentation/widget/Tile-widget.dart';

class FaqPage extends StatefulWidget {
  const FaqPage({Key? key}) : super(key: key);

  @override
  State<FaqPage> createState() => _FaqPageState();
}

class _FaqPageState extends State<FaqPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          "FAQ",
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontFamily: "Inter",
            fontWeight: FontWeight.w700,
          ),
        ),
        centerTitle: true,
        leading: Icon(
          Icons.arrow_back_ios_new_rounded,
          color: Colors.black,
        ),
      ),
      body: Column(
        children: [
          TileWidget(
              judulTile: "Cara Mengganti Nama",
              deskTile: "Lorem ipsum sit almet domet placeholder",
              isiTile:
                  "1. Tekan ini, lalu menuju ini lalu isi itu, jangan lupa save"),
          TileWidget(
              judulTile: "Cara Mengganti Password",
              deskTile: "Lorem ipsum sit almet domet placeholder",
              isiTile:
                  "1. Tekan ini, lalu menuju ini lalu isi itu, jangan lupa save"),
        ],
      ),
    );
  }
}
