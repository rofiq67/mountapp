import 'package:flutter/material.dart';
import 'package:mountapp/features/settings_page/presentation/widget/card-history-tiket.dart';

class RiwayatTiket extends StatelessWidget {
  const RiwayatTiket({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: const Size(double.maxFinite, 148),
            child: AppBar(
              leading: const Icon(
                Icons.arrow_back_ios_new_outlined,
                color: Colors.black,
              ),
              title: const Text(
                "Tiket",
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                  fontFamily: "Inter",
                  fontWeight: FontWeight.w700,
                ),
              ),
              centerTitle: true,
              flexibleSpace: Image.asset(
                'assets/images/bg gunung appbar.png',
              ),
              elevation: 0,
              backgroundColor: const Color(0xFFA6E3A7),
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(16),
                      bottomRight: Radius.circular(16))),
              actions: const [
                Padding(
                  padding: EdgeInsets.only(right: 16.0),
                  child: Icon(
                    Icons.filter_alt_outlined,
                    color: Colors.black,
                  ),
                )
              ],
              // surfaceTintColor: Colors.black,

              bottom: TabBar(
                  labelPadding: const EdgeInsets.only(bottom: 39),
                  labelStyle: const TextStyle(
                    color: Color(0x7f000000),
                    fontSize: 13,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w600,
                  ),
                  unselectedLabelStyle: const TextStyle(
                    color: Color(0x7f000000),
                    fontSize: 13,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w600,
                  ),
                  // indicatorPadding: const EdgeInsets.only(top: 25),
                  indicatorColor: Colors.black,
                  // indicatorSize: ,
                  labelColor: Colors.black,
                  splashBorderRadius: BorderRadius.circular(8),
                  // unselectedLabelColor: Colors.transparent,
                  // indicator: const BoxDecoration(
                  //     borderRadius: BorderRadius.only(
                  //         topLeft: Radius.circular(8),
                  //         bottomLeft: Radius.circular(8))),
                  tabs: const [
                    Tab(
                      text: 'Tersedia',
                    ),
                    Tab(
                      text: 'Selesai',
                    )
                  ]),
            ),
          ),
          body: TabBarView(
            children: [
              Column(
                children: [
                  SizedBox(
                    height: 8,
                  ),
                  CardHistoryTiket(
                    tipeTiket: "Tiket Gunung",
                    judulTiket: "Gunung Merbabu via Selo",
                    checkIn: "Sel, 25 Mei 2022",
                    checkOut: "Jum, 28 Mei 2022",
                  ),
                  CardHistoryTiket(
                    tipeTiket: "Open Trip",
                    judulTiket: "Muncak Bareng neng Sumeru",
                    checkIn: "Sel, 25 Mei 2022",
                    checkOut: "Jum, 28 Mei 2022",
                  ),
                ],
              ),
              Column(
                children: [
                  SizedBox(
                    height: 8,
                  ),
                  CardHistoryTiket(
                    tipeTiket: "Tiket Gunung",
                    judulTiket: "Gunung Merbabu via Selo",
                    checkIn: "Sel, 25 Mei 2022",
                    checkOut: "Jum, 28 Mei 2022",
                  ),
                  CardHistoryTiket(
                    tipeTiket: "Open Trip",
                    judulTiket: "Muncak Bareng neng Sumeru",
                    checkIn: "Sel, 25 Mei 2022",
                    checkOut: "Jum, 28 Mei 2022",
                  ),
                  CardHistoryTiket(
                    tipeTiket: "Tiket Gunung",
                    judulTiket: "Gunung Merbabu via Selo",
                    checkIn: "Sel, 25 Mei 2022",
                    checkOut: "Jum, 28 Mei 2022",
                  ),
                ],
              ),
            ],
          )), //
    );
  }
}
