import 'package:flutter/material.dart';
import 'package:mountapp/features/feed_page/presentation/pages/galeri_feed.dart';
import 'package:mountapp/features/feed_page/presentation/pages/trending_feed.dart';
import 'package:mountapp/features/feed_page/presentation/pages/video_feed.dart';
// import 'package:mountapp/features/info_gunung/presentations/pages/galeri_view.dart';

class Feedpage extends StatefulWidget {
  const Feedpage({Key? key}) : super(key: key);

  @override
  State<Feedpage> createState() => _FeedpageState();
}

class _FeedpageState extends State<Feedpage> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(110),
          child: AppBar(
            backgroundColor: Colors.white,
            elevation: 1,
            centerTitle: true,
            title: Container(
              margin: const EdgeInsets.only(top: 8),
              width: double.maxFinite,
              height: 45,
              child: TextField(
                decoration: InputDecoration(
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 13, horizontal: 16),
                  fillColor: Color(0xffEAEAEA),
                  filled: true,
                  focusColor: Color(0xffF9F9F9),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                    borderSide: BorderSide.none,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                    borderSide: BorderSide(
                      color: Color(0xff03AC0E),
                      width: 2.0,
                    ),
                  ),
                  suffixIcon: Icon(
                    Icons.search_rounded,
                    // color: Color(0xffeaeaea),
                  ),
                  suffixIconColor: Colors.black45,
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)),
                ),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: [BoxShadow(blurRadius: 4, spreadRadius: -2)]),
            ),
            bottom: TabBar(
                labelColor: Colors.black,
                indicatorColor: Color(0xff03AC0E),
                tabs: [
                  Tab(
                    text: "Trending",
                  ),
                  Tab(
                    text: "Terbaru",
                  ),
                  Tab(
                    text: "Foto",
                  ),
                  Tab(
                    text: "Video",
                  ),
                ]),
          ),
        ),
        backgroundColor: Colors.white,
        body: TabBarView(
          children: [TrendingFeed(), TrendingFeed(), GaleriFeed(), VideoFeed()],
        ),
      ),
    );
  }
}
