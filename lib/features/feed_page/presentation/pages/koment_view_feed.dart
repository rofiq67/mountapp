import 'package:flutter/material.dart';

class KomenViewFeed extends StatefulWidget {
  const KomenViewFeed({Key? key}) : super(key: key);

  @override
  State<KomenViewFeed> createState() => _KomenViewFeedState();
}

class _KomenViewFeedState extends State<KomenViewFeed> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: Icon(
          Icons.arrow_back_ios_new_rounded,
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 16.0, left: 16),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CircleAvatar(),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text(
                          "Linda",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                            fontFamily: "Inter",
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Text(
                          "ID : 7357491",
                          style: TextStyle(
                            color: Color(0x7f000000),
                            fontSize: 10,
                            fontFamily: "Inter",
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 135,
                      ),
                      ElevatedButton(
                        onPressed: () {},
                        child: Center(
                          child: Text(
                            "Follow",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 8,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                            minimumSize: const Size(58, 17),
                            maximumSize: const Size(58, 17),
                            backgroundColor: Color(0xff81D587),
                            elevation: 0,
                            foregroundColor: const Color(0xff03AC0E)),
                      ),
                      Icon(Icons.more_vert_outlined)
                    ],
                  ),
                  SizedBox(
                    width: 327,
                    height: 93,
                    child: Text(
                      "sangat melelahkan mendaki gunung semeru tetapi terbayar sudah dengan keindahan gunung semeru\n\n#GunungSemeru",
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: Image.network(
                        "https://th.bing.com/th/id/OIP.eWGN1ZU8QhcL8iz7e3j2AAHaE7?pid=ImgDet&rs=1",
                        fit: BoxFit.fill,
                        width: 324,
                        height: 330,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4),
                    child: Text(
                      "22 April 2022, 16.53",
                      style: TextStyle(
                        color: Color(0x7f000000),
                        fontSize: 10,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 8),
              width: double.maxFinite,
              height: 1,
              color: Colors.black54,
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 8.0,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "12,345",
                    style: TextStyle(fontSize: 13, fontWeight: FontWeight.w700),
                  ),
                  Text(
                    "Suka",
                    style: TextStyle(
                      fontSize: 13,
                      color: Color(0x7f000000),
                    ),
                  ),
                  Icon(
                    Icons.favorite,
                    color: Colors.red,
                  ),
                  SizedBox(
                    width: 50,
                  ),
                  Text(
                    "1",
                    style: TextStyle(fontSize: 13, fontWeight: FontWeight.w700),
                  ),
                  Text(
                    "Komentar",
                    style: TextStyle(
                      fontSize: 13,
                      color: Color(0x7f000000),
                    ),
                  ),
                  Icon(
                    Icons.comment_outlined,
                    color: Colors.black,
                  )
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 8),
              width: double.maxFinite,
              height: 1,
              color: Colors.black54,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0, left: 16),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CircleAvatar(),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0, left: 8),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              "Linda",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 4.0),
                              child: Text(
                                "ID : 7357491",
                                style: TextStyle(
                                  color: Color(0x7f000000),
                                  fontSize: 10,
                                  fontFamily: "Inter",
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            )
                          ],
                        ),
                        Text(
                          "Mengomentari . 9 jam",
                          style: TextStyle(
                            color: Color(0x7f000000),
                            fontSize: 10,
                            fontFamily: "Inter",
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 8),
                          width: 254,
                          child: Text(
                            "Hai, Salam kenal. Saya Alex dari Secang",
                            style: TextStyle(
                              fontSize: 10,
                            ),
                          ),
                        ),
                        //
                        Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 6.0, horizontal: 8),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Icon(
                                    Icons.favorite,
                                    color: Colors.red,
                                    size: 20,
                                  ),
                                  Text(
                                    "12",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 13,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 4.0),
                                    child: Icon(
                                      Icons.mode_comment_outlined,
                                      size: 20,
                                    ),
                                  ),
                                  Text(
                                    "2",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 13,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 130,
                                  ),
                                  Icon(Icons.more_horiz_rounded)
                                ]))
                      ],
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {},
                    child: Center(
                      child: Text(
                        "Follow",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 8,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                        minimumSize: const Size(58, 17),
                        maximumSize: const Size(58, 17),
                        backgroundColor: Color(0xff81D587),
                        elevation: 0,
                        foregroundColor: const Color(0xff03AC0E)),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 8),
              width: double.maxFinite,
              height: 1,
              color: Colors.black54,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0, left: 16),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CircleAvatar(),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0, left: 8),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              "Linda",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 4.0),
                              child: Text(
                                "ID : 7357491",
                                style: TextStyle(
                                  color: Color(0x7f000000),
                                  fontSize: 10,
                                  fontFamily: "Inter",
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            )
                          ],
                        ),
                        Text(
                          "Mengomentari . 9 jam",
                          style: TextStyle(
                            color: Color(0x7f000000),
                            fontSize: 10,
                            fontFamily: "Inter",
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(top: 8),
                          width: 254,
                          child: Text(
                            "Hai, Salam kenal. Saya Alex dari Secang",
                            style: TextStyle(
                              fontSize: 10,
                            ),
                          ),
                        ),
                        //
                        Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 6.0, horizontal: 8),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Icon(
                                    Icons.favorite,
                                    color: Colors.red,
                                    size: 20,
                                  ),
                                  Text(
                                    "12",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 13,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 4.0),
                                    child: Icon(
                                      Icons.mode_comment_outlined,
                                      size: 20,
                                    ),
                                  ),
                                  Text(
                                    "2",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 13,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 130,
                                  ),
                                  Icon(Icons.more_horiz_rounded)
                                ]))
                      ],
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {},
                    child: Center(
                      child: Text(
                        "Follow",
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 8,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                        minimumSize: const Size(58, 17),
                        maximumSize: const Size(58, 17),
                        backgroundColor: Color(0xff81D587),
                        elevation: 0,
                        foregroundColor: const Color(0xff03AC0E)),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
