import 'package:flutter/material.dart';

class VideoFeed extends StatefulWidget {
  const VideoFeed({Key? key}) : super(key: key);

  @override
  State<VideoFeed> createState() => _VideoFeedState();
}

class _VideoFeedState extends State<VideoFeed> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16),
          child: Row(
            children: [
              Column(
                children: [
                  Container(
                    margin:
                        const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                    width: 172,
                    height: 172,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.amber),
                    child: FlutterLogo(size: 162),
                  ),
                  Container(
                    margin:
                        const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                    width: 172,
                    height: 172,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.orange),
                    child: FlutterLogo(size: 162),
                  ),
                  Container(
                    margin:
                        const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                    width: 172,
                    height: 105,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.red),
                    child: FlutterLogo(size: 162),
                  ),
                  Container(
                    margin:
                        const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                    width: 172,
                    height: 172,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.blue),
                    child: FlutterLogo(size: 162),
                  )
                ],
              ),
              Column(
                children: [
                  Container(
                    margin:
                        const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                    width: 172,
                    height: 302,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.green),
                    child: FlutterLogo(size: 162),
                  ),
                  Container(
                    margin:
                        const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                    width: 172,
                    height: 172,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.blue),
                    child: FlutterLogo(size: 162),
                  ),
                  Container(
                    margin:
                        const EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                    width: 172,
                    height: 172,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.blue),
                    child: FlutterLogo(size: 162),
                  )
                ],
              )
            ],
          )),
    );
  }
}
