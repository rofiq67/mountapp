import 'package:flutter/material.dart';

class TrendingFeed extends StatefulWidget {
  const TrendingFeed({Key? key}) : super(key: key);

  @override
  State<TrendingFeed> createState() => _TrendingFeedState();
}

class _TrendingFeedState extends State<TrendingFeed> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            child: Column(children: [
          Padding(
            padding: const EdgeInsets.only(
              top: 16.0,
              left: 16.0,
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0, left: 8),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Linda",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: Text(
                              "ID : 7357491",
                              style: TextStyle(
                                color: Color(0x7f000000),
                                fontSize: 10,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      Text(
                        "9 jam",
                        style: TextStyle(
                          color: Color(0x7f000000),
                          fontSize: 10,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        width: 254,
                        child: Text(
                          "sangat melelahkan mendaki gunung semeru tetapi terbayar sudah dengan keindahan gunung semeru\n\n#GunungSemeru",
                          style: TextStyle(
                            fontSize: 10,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(8),
                          child: Image.network(
                            "https://th.bing.com/th/id/OIP.eWGN1ZU8QhcL8iz7e3j2AAHaE7?pid=ImgDet&rs=1",
                            fit: BoxFit.fill,
                            width: 256,
                            height: 214,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 6.0, horizontal: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Icon(
                              Icons.favorite,
                              color: Colors.red,
                              size: 20,
                            ),
                            Text(
                              "12,354",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 13,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 4.0),
                              child: Icon(
                                Icons.mode_comment_outlined,
                                size: 20,
                              ),
                            ),
                            Text(
                              "2",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 13,
                              ),
                            ),
                            SizedBox(
                              width: 105,
                            ),
                            PopupMenuButton(
                                child: Icon(Icons.more_horiz_rounded),
                                itemBuilder: (context) {
                                  return [
                                    PopupMenuItem(
                                        child: Container(
                                      width: 134,
                                      height: 102,
                                      child: Column(
                                        children: [
                                          ElevatedButton(
                                            onPressed: () {},
                                            child: Row(
                                              children: [
                                                Text(
                                                  "Bagikan",
                                                  style: TextStyle(
                                                    color: Color(0xb2000000),
                                                    fontSize: 12,
                                                  ),
                                                ),
                                                Icon(
                                                  Icons.share,
                                                  color: Colors.black,
                                                  size: 13,
                                                )
                                              ],
                                            ),
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor: Colors.white,
                                              elevation: 0,
                                              // minimumSize:
                                              //     const Size(double.maxFinite, 15),
                                              // maximumSize:
                                              //     const Size(double.maxFinite, 15),
                                            ),
                                          ),
                                          ElevatedButton(
                                            onPressed: () {},
                                            child: Row(
                                              children: [
                                                Text(
                                                  "Bagikan",
                                                  style: TextStyle(
                                                    color: Color(0xb2000000),
                                                    fontSize: 12,
                                                  ),
                                                ),
                                                Icon(
                                                  Icons.share,
                                                  color: Colors.black,
                                                  size: 13,
                                                )
                                              ],
                                            ),
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor: Colors.white,
                                              elevation: 0,
                                              // minimumSize:
                                              //     const Size(double.maxFinite, 30),
                                              // maximumSize:
                                              //     const Size(double.maxFinite, 15),
                                            ),
                                          ),
                                          ElevatedButton(
                                            onPressed: () {},
                                            child: Row(
                                              children: [
                                                Text(
                                                  "Bagikan",
                                                  style: TextStyle(
                                                    color: Color(0xb2000000),
                                                    fontSize: 12,
                                                  ),
                                                ),
                                                Icon(
                                                  Icons.share,
                                                  color: Colors.black,
                                                  size: 13,
                                                )
                                              ],
                                            ),
                                            style: ElevatedButton.styleFrom(
                                              backgroundColor: Colors.white,
                                              elevation: 0,
                                              // minimumSize:
                                              //     const Size(double.maxFinite, 0),
                                              // maximumSize:
                                              //     const Size(double.maxFinite, 0),
                                            ),
                                          )
                                        ],
                                      ),
                                    ))
                                  ];
                                })
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                ElevatedButton(
                  onPressed: () {},
                  child: Center(
                    child: Text(
                      "Follow",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  style: ElevatedButton.styleFrom(
                      minimumSize: const Size(58, 17),
                      maximumSize: const Size(58, 17),
                      backgroundColor: Color(0xff81D587),
                      elevation: 0,
                      foregroundColor: const Color(0xff03AC0E)),
                ),
                // Icon(Icons.more_vert_rounded)
              ],
            ),
          ),
          Container(
            width: double.maxFinite,
            height: 1,
            color: Colors.black54,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0, left: 16),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0, left: 8),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Linda",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: Text(
                              "ID : 7357491",
                              style: TextStyle(
                                color: Color(0x7f000000),
                                fontSize: 10,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      Text(
                        "9 jam",
                        style: TextStyle(
                          color: Color(0x7f000000),
                          fontSize: 10,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        width: 254,
                        child: Text(
                          "Hai, Salam kenal. Saya Alex dari Secang",
                          style: TextStyle(
                            fontSize: 10,
                          ),
                        ),
                      ),
                      //
                      Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 6.0, horizontal: 8),
                          child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Icon(
                                  Icons.favorite,
                                  color: Colors.red,
                                  size: 20,
                                ),
                                Text(
                                  "12",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 13,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 4.0),
                                  child: Icon(
                                    Icons.mode_comment_outlined,
                                    size: 20,
                                  ),
                                ),
                                Text(
                                  "2",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 13,
                                  ),
                                ),
                                SizedBox(
                                  width: 130,
                                ),
                                Icon(Icons.more_horiz_rounded)
                              ]))
                    ],
                  ),
                ),
                ElevatedButton(
                  onPressed: () {},
                  child: Center(
                    child: Text(
                      "Follow",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  style: ElevatedButton.styleFrom(
                      minimumSize: const Size(58, 17),
                      maximumSize: const Size(58, 17),
                      backgroundColor: Color(0xff81D587),
                      elevation: 0,
                      foregroundColor: const Color(0xff03AC0E)),
                ),
              ],
            ),
          ),
          Container(
            width: double.maxFinite,
            height: 1,
            color: Colors.black54,
          ),

          ///
          //////
          ///
          ///
          Padding(
              padding: const EdgeInsets.only(
                top: 16.0,
                left: 16.0,
              ),
              child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CircleAvatar(),
                    Padding(
                      padding: const EdgeInsets.only(top: 4.0, left: 8),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Linda",
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontFamily: "Inter",
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 4.0),
                                child: Text(
                                  "ID : 7357491",
                                  style: TextStyle(
                                    color: Color(0x7f000000),
                                    fontSize: 10,
                                    fontFamily: "Inter",
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              )
                            ],
                          ),
                          Text(
                            "9 jam",
                            style: TextStyle(
                              color: Color(0x7f000000),
                              fontSize: 10,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          Container(
                            margin: const EdgeInsets.only(top: 8),
                            width: 254,
                            child: Text(
                              "sangat melelahkan mendaki gunung semeru tetapi terbayar sudah dengan keindahan gunung semeru\n\n#GunungSemeru",
                              style: TextStyle(
                                fontSize: 10,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(8),
                              child: Image.network(
                                "https://media-cdn.tripadvisor.com/media/photo-s/16/c6/57/c9/one-of-our-best-photos.jpg",
                                fit: BoxFit.fill,
                                width: 256,
                                height: 214,
                              ),
                            ),
                          ),
                          Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 6.0, horizontal: 8),
                              child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Icon(
                                      Icons.favorite,
                                      color: Colors.red,
                                      size: 20,
                                    ),
                                    Text(
                                      "12,354",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 13,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 4.0),
                                      child: Icon(
                                        Icons.mode_comment_outlined,
                                        size: 20,
                                      ),
                                    ),
                                    Text(
                                      "2",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 13,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 105,
                                    ),
                                  ]))
                        ],
                      ),
                    ),
                  ]))
        ])));
  }
}
