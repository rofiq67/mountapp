import 'package:flutter/material.dart';
import 'package:mountapp/features/artikel_page/presentations/widgets/banner-artikel.dart';
import 'package:mountapp/features/artikel_page/presentations/widgets/daftar-list-artikel.dart';

class ArtikelList extends StatelessWidget {
  const ArtikelList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: Icon(
          Icons.arrow_back_ios_new_rounded,
          color: Colors.black,
        ),
        title: Container(
            width: 246,
            height: 32,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: const Color(0xFFF9F9F9),
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x3f000000),
                    blurRadius: 4,
                    offset: Offset(0, 0),
                  )
                ]),
            child: const Center(
              child: TextField(
                decoration: InputDecoration(
                    suffixIcon: Icon(
                      Icons.search,
                      color: Color(0x7f7f3a44),
                    ),
                    border: InputBorder.none),
              ),
            )),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 16),
              child: Text(
                "Rekomendasi untuk anda",
                style: TextStyle(
                  color: Color(0xff03ac0e),
                  fontSize: 18,
                  fontFamily: "Inter",
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            Container(
              height: 159,
              child: ListView(scrollDirection: Axis.horizontal, children: [
                ViewBannerArtikel(
                  gambarArtikel:
                      "https://www.exploregunung.net/wp/wp-content/uploads/2015/02/gunung-sumbing.jpg",
                  judulArtikel:
                      "Tips Mendaki Gunung di Musim Kemarau, Awas Rawan Kebakaran!",
                ),
                ViewBannerArtikel(
                  gambarArtikel:
                      "https://yayasanpalung.files.wordpress.com/2016/07/buah-buahan-hutan-termasuk-sumber-makanan-utama-pakan-orangutan-foto-dok-tim-laman-dan-yayasan-palung.jpg?w=677",
                  judulArtikel: "Survival Tips : Makanan dari Hutan",
                ),
                ViewBannerArtikel(
                  gambarArtikel:
                      "https://asset.kompas.com/crops/YH_65pUqj5OqPRg0RodeBrINf0E=/0x0:0x0/750x500/data/photo/2021/08/21/61209f762d68c.jpg",
                  judulArtikel: "Pertolongan Pertama ketika Survive",
                ),
                ViewBannerArtikel(
                  gambarArtikel:
                      "https://www.exploregunung.net/wp/wp-content/uploads/2015/02/gunung-sumbing.jpg",
                  judulArtikel:
                      "Tips Mendaki Gunung di Musim Kemarau, Awas Rawan Kebakaran!",
                ),
              ]),
            ),
            SizedBox(
              height: 32,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Hari ini",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontFamily: "Inter",
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Center(
                    child: Container(
                      width: double.maxFinite,
                      height: 3,
                      color: Colors.black,
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  DaftarListArtikel(
                    judulDaftarArt:
                        "Tips Mendaki Gunung di Musim Kemarau, Awas Rawan Kebakaran!",
                    gambarDaftarArt:
                        "https://asset.kompas.com/crops/YH_65pUqj5OqPRg0RodeBrINf0E=/0x0:0x0/750x500/data/photo/2021/08/21/61209f762d68c.jpg",
                    viewDaftarArt: 12,
                  ),
                  DaftarListArtikel(
                    judulDaftarArt: "Apa Aja yang Perlu dibawa Saat Mendaki?",
                    gambarDaftarArt:
                        "https://4.bp.blogspot.com/-8M0yUY_hzpw/WcY_6Upw_iI/AAAAAAAAC1Y/fTFimOfUtAgXyIjZX9J1WW8yVNdwo73pACLcBGAs/s640/Perlengkapan-Mendaki-Gunung-Bagi-Pemula-Beautyinthebackcountry.com_.jpg",
                    viewDaftarArt: 8,
                  ),
                  DaftarListArtikel(
                    judulDaftarArt:
                        "5 Tips Mengatasi Serangan Lintah Saat Mendaki Gunung",
                    gambarDaftarArt:
                        "https://media.istockphoto.com/id/825443974/id/vektor/serangan-lintah.jpg?s=612x612&w=0&k=20&c=YEcjlmygHY6xgum_GY91KBC0kjEux7AffRFY0ZP_TFA=",
                    viewDaftarArt: 75,
                  ),
                  DaftarListArtikel(
                    judulDaftarArt:
                        "5 Tips Mengatasi Serangan Lintah Saat Mendaki Gunung",
                    gambarDaftarArt:
                        "https://media.istockphoto.com/id/825443974/id/vektor/serangan-lintah.jpg?s=612x612&w=0&k=20&c=YEcjlmygHY6xgum_GY91KBC0kjEux7AffRFY0ZP_TFA=",
                    viewDaftarArt: 100,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
