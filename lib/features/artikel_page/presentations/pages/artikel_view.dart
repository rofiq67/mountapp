import 'package:flutter/material.dart';

class ArtikelView extends StatelessWidget {
  const ArtikelView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: Icon(
          Icons.arrow_back_ios_new_rounded,
          color: Colors.black,
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: Icon(
              Icons.share,
              color: Colors.black,
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Column(
                children: [
                  SizedBox(
                    width: 320,
                    child: Text(
                      "Tips Mendaki Gunung di Musim Kemarau, Awas Rawan Kebakaran!",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Container(
                    width: double.maxFinite,
                    height: 274,
                    child: Image.network(
                      'https://1.bp.blogspot.com/-cN7ahb1hR5s/W-fgYCM-XiI/AAAAAAAAAH0/NmhLoIoXzjMPfqYd980Hoird3oIzebkIgCLcBGAs/w1200-h630-p-k-no-nu/0qDhQ2.jpg',
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              width: double.maxFinite,
              child: Column(
                children: [
                  Text(
                    "Musim kemarau merupakan kondisi terbaik yang paling dinanti para pendaki untuk bisa mendaki gunung. Sebab, potensi hujan yang sedikit membuat aktivitas mendaki jadi lebih mudah untuk dilakukan. Selain itu, cuaca cerah juga membuat pemandangan di sekitar trek jadi lebih indah, apalagi kalau lo mengincar momen matahari terbit Superfriends.s\n\nMeski demikian, musim kemarau gak sepenuhnya aman dan nyaman karena pada musim ini juga rawan terjadi kebakaran hutan. Nah, untuk itu sebaiknya ketahui tips mendaki gunung saat musim kemarau berikut ini agar aktivitas pendakian lo terasa lebih menyenangkan. Yuk, disimak!",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                    ),
                  ),
                  Text(
                    "\nMusim kemarau merupakan kondisi terbaik yang paling dinanti para pendaki untuk bisa mendaki gunung. Sebab, potensi hujan yang sedikit membuat aktivitas mendaki jadi lebih mudah untuk dilakukan. Selain itu, cuaca cerah juga membuat pemandangan di sekitar trek jadi lebih indah, apalagi kalau lo mengincar momen matahari terbit Superfriends.s\n\nMeski demikian, musim kemarau gak sepenuhnya aman dan nyaman karena pada musim ini juga rawan terjadi kebakaran hutan. Nah, untuk itu sebaiknya ketahui tips mendaki gunung saat musim kemarau berikut ini agar aktivitas pendakian lo terasa lebih menyenangkan. Yuk, disimak!",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
