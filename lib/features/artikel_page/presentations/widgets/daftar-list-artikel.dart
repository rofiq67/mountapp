import 'package:flutter/material.dart';

class DaftarListArtikel extends StatelessWidget {
  final String judulDaftarArt;
  final String gambarDaftarArt;
  final int viewDaftarArt;
  const DaftarListArtikel(
      {required this.judulDaftarArt,
      required this.gambarDaftarArt,
      required this.viewDaftarArt,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 16),
      width: double.maxFinite,
      height: 118,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border(bottom: BorderSide(color: Colors.black))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            // mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: 211,
                child: Text(
                  judulDaftarArt,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "Baca Selengkapnya",
                    style: TextStyle(
                      color: Color(0x7f000000),
                      fontSize: 8,
                    ),
                  ),
                  SizedBox(
                    width: 64,
                  ),
                  Row(
                    children: [
                      Icon(
                        Icons.remove_red_eye_outlined,
                        color: Color(0x7f000000),
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(
                        "$viewDaftarArt views",
                        style: TextStyle(
                          color: Color(0x7f000000),
                          fontSize: 8,
                        ),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
          SizedBox(
            width: 8,
          ),
          Container(
            width: 120,
            height: 103,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(6),
              child: Image.network(
                  'https://www.exploregunung.net/wp/wp-content/uploads/2015/02/gunung-sumbing.jpg',
                  fit: BoxFit.fill,
                  width: 120,
                  height: 103),
            ),
          )
        ],
      ),
    );
  }
}
