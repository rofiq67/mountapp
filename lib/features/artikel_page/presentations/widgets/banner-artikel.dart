import 'package:flutter/material.dart';

class ViewBannerArtikel extends StatelessWidget {
  final String gambarArtikel;
  final String judulArtikel;
  const ViewBannerArtikel(
      {required this.gambarArtikel, required this.judulArtikel, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Container(
        // margin: const EdgeInsets.all(16),
        width: 261,
        height: 159,
        decoration: BoxDecoration(
          color: Color(0xff03ac0e),
          borderRadius: BorderRadius.circular(16),
        ),
        child: Stack(children: [
          Container(
            width: 261,
            height: 159,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(16),
              child: Image.network(
                gambarArtikel,
                fit: BoxFit.fill,
                width: 261,
                height: 159,
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                height: 30,
              ),
              Center(
                child: SizedBox(
                  width: 181,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      judulArtikel,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 13,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ]),
      ),
    );
  }
}
