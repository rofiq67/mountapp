import 'package:flutter/material.dart';
import 'package:mountapp/core/components/bg_appbar.dart';

class KonfirmasiTiket extends StatefulWidget {
  const KonfirmasiTiket({Key? key}) : super(key: key);

  @override
  State<KonfirmasiTiket> createState() => _KonfirmasiTiketState();
}

class _KonfirmasiTiketState extends State<KonfirmasiTiket> {
  bool isSwitched = false;
  bool? check2 = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Stack(
        children: [
          BgAppbar(
            judulAppbar: "Ringkasan",
            iconAct: null,
          ),
          Column(
            children: [
              Container(
                  width: double.maxFinite,
                  height: 1150,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16.0),
                    child: Column(children: [
                      Stack(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 91),
                            child: Container(
                              width: double.maxFinite,
                              height: 160,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(16),
                                boxShadow: const [
                                  BoxShadow(
                                    color: Color(0x3f000000),
                                    blurRadius: 4,
                                    offset: Offset(0, 2),
                                  ),
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16, vertical: 16),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    const Text(
                                      "Detail Pemesanan",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 15,
                                        fontFamily: "Inter",
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 5),
                                      child: Row(
                                        children: [
                                          Container(
                                            width: 60,
                                            height: 60,
                                            child: ClipRRect(
                                              borderRadius:
                                                  BorderRadius.circular(6),
                                              child: Image.asset(
                                                "assets/images/gunung_sumeru.png",
                                              ),
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 16.0),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: const [
                                                Text(
                                                  "Gunung Merbabu, Via Selo",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 14,
                                                    fontFamily: "Inter",
                                                    fontWeight: FontWeight.w700,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 20,
                                                  height: 20,
                                                ),
                                                Text(
                                                  "3426 Mdpl",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Color(0x7f000000),
                                                    fontSize: 12,
                                                  ),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 5),
                                      child: Column(
                                        children: [
                                          Row(
                                            mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceAround,
                                            children: const [
                                              Text(
                                                "Check-in",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12,
                                                ),
                                              ),
                                              SizedBox(
                                                width: 180,
                                                height: 5,
                                              ),
                                              Text(
                                                "Sel, 26 Mei 2022",
                                                textAlign: TextAlign.center,
                                                style: TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 12,
                                                  fontFamily: "Inter",
                                                  fontWeight: FontWeight.w500,
                                                ),
                                              )
                                            ],
                                          ),
                                          Padding(
                                            padding:
                                                const EdgeInsets.only(top: 6.0),
                                            child: Row(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: const [
                                                Text(
                                                  "Check-out",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 12,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 160,
                                                  height: 5,
                                                ),
                                                Text(
                                                  "Rabu, 29 Mei 2022",
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 12,
                                                    fontFamily: "Inter",
                                                    fontWeight: FontWeight.w500,
                                                  ),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          )
                        ],
                      ),

                      ////tambah pendaki
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Detail Pendakian",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text(
                                  "Saya tidak mendaki sendirian",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Color(0x7f000000),
                                    fontSize: 12,
                                    fontFamily: "Inter",
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                const SizedBox(
                                  width: 143,
                                  height: 10,
                                ),
                                Switch(
                                  value: isSwitched,
                                  onChanged: (value) {
                                    setState(() {
                                      isSwitched = value;
                                      print(isSwitched);
                                    });
                                  },
                                  activeColor: Colors.green,
                                  activeTrackColor: Colors.lightGreenAccent,
                                )
                              ],
                            ),
                            Container(
                              width: double.maxFinite,
                              height: 45,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                // color: const
                              ),
                              child: ElevatedButton(
                                onPressed: (() {
                                  showModalBottomSheet(
                                      context: context,
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(40),
                                            topRight: Radius.circular(40)),
                                      ),
                                      builder: (context) {
                                        return Container(
                                          width: double.maxFinite,
                                          height: 934,

                                          // decoration: const BoxDecoration(
                                          //     borderRadius: BorderRadius.only(
                                          //         topLeft: Radius.circular(16),
                                          //         topRight: Radius.circular(16))),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Center(
                                                child: Container(
                                                  margin: const EdgeInsets.only(
                                                      top: 18),
                                                  width: 99,
                                                  height: 4,
                                                  decoration: BoxDecoration(
                                                    border: Border.all(
                                                      color: const Color(
                                                          0xffdfdfdf),
                                                      width: 3,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        horizontal: 16.0),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    const Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 32.0),
                                                      child: Text(
                                                        "Tambah Pendaki",
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 20,
                                                          fontFamily: "Inter",
                                                          fontWeight:
                                                              FontWeight.w700,
                                                        ),
                                                      ),
                                                    ),
                                                    const Padding(
                                                      padding: EdgeInsets.only(
                                                          top: 4.0),
                                                      child: Text(
                                                        "Masukan id Pendaki untuk menambahkan",
                                                        style: TextStyle(
                                                          color:
                                                              Color(0x7f000000),
                                                          fontSize: 12,
                                                          fontFamily: "Inter",
                                                          fontWeight:
                                                              FontWeight.w500,
                                                        ),
                                                      ),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              top: 16),
                                                      child: TextFormField(
                                                        decoration:
                                                            InputDecoration(
                                                                fillColor: const Color(
                                                                    0xffe9e9e9),
                                                                filled: true,
                                                                hintText:
                                                                    "Masukan Id user",
                                                                hintStyle:
                                                                    const TextStyle(
                                                                  color: Color(
                                                                      0x7f000000),
                                                                  fontSize: 12,
                                                                ),
                                                                contentPadding:
                                                                    const EdgeInsets
                                                                            .symmetric(
                                                                        vertical:
                                                                            12.5,
                                                                        horizontal:
                                                                            16),
                                                                border: OutlineInputBorder(
                                                                    borderSide:
                                                                        BorderSide
                                                                            .none,
                                                                    borderRadius:
                                                                        BorderRadius.circular(
                                                                            16))),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                        );
                                      });
                                }),
                                style: ElevatedButton.styleFrom(
                                  minimumSize: const Size(double.maxFinite, 45),
                                  maximumSize: const Size(double.maxFinite, 45),
                                  backgroundColor: const Color(0xffe9e9e9),
                                  // shadowColor:
                                  //     Colors.grey.withOpacity(0.5),
                                  elevation: 0,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 8,
                                  ),
                                  child: Row(
                                    children: const [
                                      Icon(
                                        Icons.person_outline_outlined,
                                        color: Colors.black45,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          "ID User",
                                          style: TextStyle(
                                            color: Color(0x7f000000),
                                            fontSize: 12,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),

                      ////transportasi
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Transportasi",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                const Text(
                                  "Pakai transportasi untuk menjemput anda",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    color: Color(0x7f000000),
                                    fontSize: 12,
                                    fontFamily: "Inter",
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                const SizedBox(
                                  width: 75,
                                  height: 10,
                                ),
                                Switch(
                                  value: isSwitched,
                                  onChanged: (value) {
                                    setState(() {
                                      isSwitched = value;
                                      print(isSwitched);
                                    });
                                  },
                                  activeColor: Colors.green,
                                  activeTrackColor: Colors.lightGreenAccent,
                                )
                              ],
                            ),
                            Container(
                              width: double.maxFinite,
                              height: 45,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                // color: const
                              ),
                              child: ElevatedButton(
                                onPressed: null,
                                style: ElevatedButton.styleFrom(
                                  minimumSize: const Size(double.maxFinite, 45),
                                  maximumSize: const Size(double.maxFinite, 45),
                                  backgroundColor: const Color(0xffe9e9e9),
                                  // shadowColor:
                                  //     Colors.grey.withOpacity(0.5),
                                  elevation: 0,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 8,
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: const [
                                      Padding(
                                        padding: EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          "Tentukan titik penjemputan anda",
                                          style: TextStyle(
                                            color: Color(0x7f000000),
                                            fontSize: 12,
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 100,
                                        height: 1,
                                      ),
                                      Icon(
                                        Icons.search_rounded,
                                        color: Colors.black45,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),

                      //jasa lainnya
                      SizedBox(
                        height: 16,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            "Jasa lainnya",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 15,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          CheckboxListTile(
                            activeColor: Colors.green,
                            value: check2,
                            contentPadding: EdgeInsets.zero,
                            controlAffinity: ListTileControlAffinity.leading,
                            onChanged: (bool? value) {
                              setState(() {
                                check2 = value;
                              });
                            },
                            title: Transform.translate(
                              offset: const Offset(-15, 7),
                              child: Row(
                                children: [
                                  const Text(
                                    "Porter",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontFamily: "Inter",
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  const SizedBox(
                                    width: 170,
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(left: 50),
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    // child:
                                  ),
                                  const Padding(
                                    padding:
                                        EdgeInsets.symmetric(horizontal: 4.0),
                                    child: Text(
                                      "2",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 12,
                                        fontFamily: "Inter",
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    width: 15,
                                    height: 15,
                                    decoration: BoxDecoration(
                                      color: Colors.green,
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                    // child:
                                  ),
                                ],
                              ),
                            ),
                            subtitle: Transform.translate(
                              offset: const Offset(-15, 5),
                              child: const Text(
                                "Lorem integer praesent etiam mi vehicula rutrum. Orci ut arcu id viverra sollicitudin. Nulla nunc, vehicula  ",
                                style: TextStyle(
                                  color: Color(0x7f000000),
                                  fontSize: 11,
                                ),
                              ),
                            ),
                          ),
                          CheckboxListTile(
                            activeColor: Colors.green,
                            value: check2,
                            contentPadding: EdgeInsets.zero,
                            controlAffinity: ListTileControlAffinity.leading,
                            onChanged: (bool? value) {
                              setState(() {
                                check2 = value;
                              });
                            },
                            title: Transform.translate(
                              offset: const Offset(-15, 7),
                              child: Row(
                                children: const [
                                  Text(
                                    "Tour Guide",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontFamily: "Inter",
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            subtitle: Transform.translate(
                              offset: const Offset(-15, 5),
                              child: const Text(
                                "Lorem integer praesent etiam mi vehicula rutrum. Orci ut arcu id viverra sollicitudin. Nulla nunc, vehicula  ",
                                style: TextStyle(
                                  color: Color(0x7f000000),
                                  fontSize: 11,
                                ),
                              ),
                            ),
                          ),
                          CheckboxListTile(
                            activeColor: Colors.green,
                            value: check2,
                            contentPadding: EdgeInsets.zero,
                            controlAffinity: ListTileControlAffinity.leading,
                            onChanged: (bool? value) {
                              setState(() {
                                check2 = value;
                              });
                            },
                            title: Transform.translate(
                              offset: const Offset(-15, 7),
                              child: Row(
                                children: const [
                                  Text(
                                    "Asuransi",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontFamily: "Inter",
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            subtitle: Transform.translate(
                              offset: const Offset(-15, 5),
                              child: const Text(
                                "Lorem integer praesent etiam mi vehicula rutrum. Orci ut arcu id viverra sollicitudin. Nulla nunc, vehicula  ",
                                style: TextStyle(
                                  color: Color(0x7f000000),
                                  fontSize: 11,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),

                      //metode pembayaran
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          vertical: 16,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text(
                              "Metode Pembayaran",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(top: 8),
                              width: double.maxFinite,
                              height: 45,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                // color: const
                              ),
                              child: ElevatedButton(
                                onPressed: (() {
                                  showModalBottomSheet(
                                      context: context,
                                      isScrollControlled: true,
                                      shape: const RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(40),
                                            topRight: Radius.circular(40)),
                                      ),
                                      builder: (context) {
                                        return Container(
                                            width: double.maxFinite,
                                            // color: const Color(0xfff5f5f5),
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.75,

                                            // decoration: const BoxDecoration(
                                            //     borderRadius: BorderRadius.only(
                                            //         topLeft: Radius.circular(16),
                                            //         topRight: Radius.circular(16))),
                                            child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Center(
                                                    child: Container(
                                                      margin:
                                                          const EdgeInsets.only(
                                                              top: 18),
                                                      width: 99,
                                                      height: 4,
                                                      decoration: BoxDecoration(
                                                        border: Border.all(
                                                          color: const Color(
                                                              0xffdfdfdf),
                                                          width: 3,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Padding(
                                                      padding: const EdgeInsets
                                                              .symmetric(
                                                          horizontal: 16.0,
                                                          vertical: 8),
                                                      child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: const [
                                                            Padding(
                                                              padding: EdgeInsets
                                                                  .only(
                                                                      top: 24),
                                                              child: Text(
                                                                "Metode Pembayaran",
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize: 20,
                                                                  fontFamily:
                                                                      "Inter",
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700,
                                                                ),
                                                              ),
                                                            ),
                                                            Padding(
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      vertical:
                                                                          8.0),
                                                              child: Text(
                                                                "Transfer Virtual Account",
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize: 15,
                                                                  fontFamily:
                                                                      "Inter",
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700,
                                                                ),
                                                              ),
                                                            ),
                                                          ])),
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      Container(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(left: 17),
                                                        width: double.maxFinite,
                                                        height: 60,
                                                        color: Colors.white,
                                                        child: Row(
                                                          mainAxisSize:
                                                              MainAxisSize.min,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Container(
                                                              width: 31,
                                                              height: 11,
                                                              color: Colors.red,
                                                            ),
                                                            const SizedBox(
                                                                width: 28),
                                                            const Text(
                                                              "BANK BRI (BRIVA)",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 12,
                                                                fontFamily:
                                                                    "Inter",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(left: 17),
                                                        width: double.maxFinite,
                                                        height: 60,
                                                        color: Colors.white,
                                                        child: Row(
                                                          mainAxisSize:
                                                              MainAxisSize.min,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Container(
                                                              width: 31,
                                                              height: 11,
                                                              color: Colors.red,
                                                            ),
                                                            const SizedBox(
                                                                width: 28),
                                                            const Text(
                                                              "BANK BRI (BRIVA)",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 12,
                                                                fontFamily:
                                                                    "Inter",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(left: 17),
                                                        width: double.maxFinite,
                                                        height: 60,
                                                        color: Colors.white,
                                                        child: Row(
                                                          mainAxisSize:
                                                              MainAxisSize.min,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Container(
                                                              width: 31,
                                                              height: 11,
                                                              color: Colors.red,
                                                            ),
                                                            const SizedBox(
                                                                width: 28),
                                                            const Text(
                                                              "BANK BRI (BRIVA)",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 12,
                                                                fontFamily:
                                                                    "Inter",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const Padding(
                                                    padding:
                                                        EdgeInsets.symmetric(
                                                            vertical: 8.0,
                                                            horizontal: 16),
                                                    child: Text(
                                                      "Gerai / Supermarket",
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 15,
                                                        fontFamily: "Inter",
                                                        fontWeight:
                                                            FontWeight.w700,
                                                      ),
                                                    ),
                                                  ),
                                                  Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      Container(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(left: 17),
                                                        width: double.maxFinite,
                                                        height: 60,
                                                        color: Colors.white,
                                                        child: Row(
                                                          mainAxisSize:
                                                              MainAxisSize.min,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Container(
                                                              width: 31,
                                                              height: 11,
                                                              color: Colors.red,
                                                            ),
                                                            const SizedBox(
                                                                width: 28),
                                                            const Text(
                                                              "Indomaret",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 12,
                                                                fontFamily:
                                                                    "Inter",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                      Container(
                                                        padding:
                                                            const EdgeInsets
                                                                .only(left: 17),
                                                        width: double.maxFinite,
                                                        height: 60,
                                                        color: Colors.white,
                                                        child: Row(
                                                          mainAxisSize:
                                                              MainAxisSize.min,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .start,
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Container(
                                                              width: 31,
                                                              height: 11,
                                                              color: Colors.red,
                                                            ),
                                                            const SizedBox(
                                                                width: 28),
                                                            const Text(
                                                              "Alfmart/Alfamidi",
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .black,
                                                                fontSize: 12,
                                                                fontFamily:
                                                                    "Inter",
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ]));
                                      });
                                }),
                                style: ElevatedButton.styleFrom(
                                  minimumSize: const Size(double.maxFinite, 45),
                                  maximumSize: const Size(double.maxFinite, 45),
                                  backgroundColor: const Color(0xffe9e9e9),
                                  // shadowColor:
                                  //     Colors.grey.withOpacity(0.5),
                                  elevation: 0,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 8,
                                  ),
                                  child: Row(children: const [
                                    Icon(
                                      Icons.payment,
                                      color: Colors.black45,
                                    ),
                                    Padding(
                                        padding: EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          "Metode Pembayaran",
                                          style: TextStyle(
                                            color: Color(0x7f000000),
                                            fontSize: 12,
                                          ),
                                        ))
                                  ]),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 8.0),
                              child: Container(
                                  padding: const EdgeInsets.only(left: 20),
                                  width: double.maxFinite,
                                  height: 40,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(6),
                                      color: Color(0xffe9e9e9)),
                                  child: Row(
                                    children: const [
                                      Icon(Icons.price_change_outlined,
                                          color: Colors.black45),
                                      Padding(
                                        padding: EdgeInsets.only(left: 8.0),
                                        child: Text(
                                          "Masukin Promo biar Hemat",
                                          style: TextStyle(
                                            color: Color(0x7f000000),
                                            fontSize: 12,
                                          ),
                                        ),
                                      )
                                    ],
                                  )),
                            ),

                            //Informasi
                            Padding(
                              padding: const EdgeInsets.only(top: 16.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    "Informasi Penting",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 15,
                                      fontFamily: "Inter",
                                      fontWeight: FontWeight.w700,
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.only(top: 8),
                                    width: 313,
                                    height: 116,
                                    child: Text(
                                      "Penggunaan tiket :\n1. Tiket ini berbentuk digital, silahkan disimpan/di screenshoot  dan tunjukkan kepada penjaga saat check-in atau check-out.\n2. Tiket hanya berlaku sampai batas check-in.\n3. Tiket tidak bisa di refound, karena kelalaian human.",
                                      style: TextStyle(
                                        color: Color(0x7f000000),
                                        fontSize: 11,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ]),
                  ))
            ],
          )
        ],
      ),
    ));
  }
}
