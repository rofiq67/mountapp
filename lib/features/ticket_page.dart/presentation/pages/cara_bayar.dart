import 'package:flutter/material.dart';
import 'package:mountapp/features/ticket_page.dart/presentation/widget/tile-cara-bayar.dart';

class CaraBayar extends StatefulWidget {
  const CaraBayar({Key? key}) : super(key: key);

  @override
  State<CaraBayar> createState() => _CaraBayarState();
}

class _CaraBayarState extends State<CaraBayar> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text(
          "Cara Pembayaran",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontFamily: "Inter",
            fontWeight: FontWeight.w700,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        leading: const Icon(
          Icons.arrow_back_ios_new_rounded,
          color: Colors.black,
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 32.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const Text(
                  "BANK BRI",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  width: 10,
                  height: 5,
                ),
                Container(
                  width: 58,
                  height: 26,
                  color: Colors.white,
                  child: Image.network(
                      'https://vignette.wikia.nocookie.net/logopedia/images/5/55/BANK_BRI.png/revision/latest/scale-to-width-down/2000?cb=20161101121011'),
                )
              ],
            ),
          ),
          TileCaraBayar(
              metodeBayar: "ATM BRI",
              noDesk: 1,
              deskBayarReguler: ". Pilih",
              deskBayarBold: " Trsansaksi Lain > Pembayaran > Lainnya > Briva"),
          TileCaraBayar(
              metodeBayar: "Mobile Banking",
              noDesk: 1,
              deskBayarReguler: ". Pilih",
              deskBayarBold: " Trsansaksi Lain > Pembayaran > Lainnya > Briva"),
          TileCaraBayar(
              metodeBayar: "Internet Banking",
              noDesk: 1,
              deskBayarReguler: ". Pilih",
              deskBayarBold: " Trsansaksi Lain > Pembayaran > Lainnya > Briva"),
        ],
      ),
    );
  }
}

// "Answers for Question One"
