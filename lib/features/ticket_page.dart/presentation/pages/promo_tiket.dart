import 'package:flutter/material.dart';

import '../widget/promo-container.dart';

class PromoTiket extends StatefulWidget {
  const PromoTiket({Key? key}) : super(key: key);

  @override
  State<PromoTiket> createState() => _PromoTiketState();
}

class _PromoTiketState extends State<PromoTiket> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Text(
          "Promo",
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontFamily: "Inter",
            fontWeight: FontWeight.w700,
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0,
        leading: const Icon(
          Icons.arrow_back_ios_rounded,
          color: Colors.black,
        ),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                margin: const EdgeInsets.only(top: 32),
                width: double.maxFinite,
                height: 41,
                child: TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                        borderSide: const BorderSide(
                          color: Color(0xff4EEE7B),
                        )),
                    hintText: "Masukan kode promo",
                    hintStyle: const TextStyle(
                      color: Color(0x7f000000),
                      fontSize: 12,
                      // focusColor: const Color(0xFF4EEE7B)
                    ),
                  ),
                )),
            SizedBox(
              height: 16,
            ),
            Text(
              "Promo Hari ini",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: 15,
                fontFamily: "Inter",
                fontWeight: FontWeight.w700,
              ),
            ),
            const Text(
              "Lorem Ipsum bigatre ipsum",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0x7f000000),
                fontSize: 12,
                fontFamily: "Inter",
                fontWeight: FontWeight.w500,
              ),
            ),
            SizedBox(
              height: 16,
            ),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
              ),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: const Color.fromARGB(255, 205, 255, 219),
                    side: const BorderSide(color: Color(0xff4EEE7B)),
                    elevation: 0,
                  ),
                  onPressed: () {
                    // print("aktip");
                  },
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 8.0, horizontal: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: const [
                            Text(
                              "Cashback Rp.500",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 14,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                            SizedBox(
                              width: 4,
                            ),
                            Text(
                              "(500 Coins)",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color(0x7f000000),
                                fontSize: 10,
                              ),
                            )
                          ],
                        ),
                        SizedBox(height: 4),
                        Row(
                          children: const [
                            Icon(
                              Icons.timer_outlined,
                              color: Color(0x7f000000),
                              size: 14,
                            ),
                            SizedBox(
                              width: 4,
                            ),
                            Text(
                              "Berakhir 5 jam lagi!",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color(0x7f000000),
                                fontSize: 10,
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  )),
            ),
            SizedBox(
              height: 16,
            ),
            PromoContainer(
              judulPromo: "Cashback Akhir Bulan",
              nilaiPromo: "(500 Coins)",
              syaratPromo: "Segera klaim promo sekarang juga!!!",
              syaratPromoBold: "",
              waktuPromo: "5",
            ),
            SizedBox(
              height: 16,
            ),
            Text(
              "Promo Kamu",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: 15,
                fontFamily: "Inter",
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(
              height: 16,
            ),
            PromoContainer(
              judulPromo: "Potongan harga nihh",
              nilaiPromo: "Rp5.000",
              syaratPromo: "Ayo",
              syaratPromoBold: " muncak Sekarang",
              waktuPromo: "1",
            ),
            SizedBox(
              height: 8,
            ),
            PromoContainer(
              judulPromo: "Cashback Akhir Bulan",
              nilaiPromo: "(500 Coins)",
              syaratPromo: "Segera klaim promo sekarang juga!!!",
              syaratPromoBold: "",
              waktuPromo: "5",
            ),
            SizedBox(
              height: 8,
            ),
            PromoContainer(
              judulPromo: "Cashback Akhir Bulan",
              nilaiPromo: "(500 Coins)",
              syaratPromo: "Segera klaim promo sekarang juga!!!",
              syaratPromoBold: "",
              waktuPromo: "5",
            ),
          ],
        ),
      ),
    );
  }
}
