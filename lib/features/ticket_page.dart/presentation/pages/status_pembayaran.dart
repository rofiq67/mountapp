// import 'dart:ffi';

import 'package:flutter/material.dart';

class StatusPembayaran extends StatefulWidget {
  const StatusPembayaran({Key? key}) : super(key: key);

  @override
  State<StatusPembayaran> createState() => _StatusPembayaranState();
}

class _StatusPembayaranState extends State<StatusPembayaran> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Transaksi",
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontFamily: "Inter",
            fontWeight: FontWeight.w700,
          ),
        ),
        centerTitle: true,
        leading: Icon(
          Icons.arrow_back_ios_rounded,
          color: Colors.black,
        ),
        elevation: 0,
        backgroundColor: Colors.transparent,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: Icon(
              Icons.filter_alt_outlined,
              color: Colors.black,
            ),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Belum dibayar",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: 16,
                fontFamily: "Inter",
                fontWeight: FontWeight.w700,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(
                top: 16,
              ),
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              width: double.maxFinite,
              height: 160,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Colors.white,
                  boxShadow: [BoxShadow(blurRadius: 4, spreadRadius: -2)]),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Bayar sebelum,",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 10,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      Text(
                        " 22 mei 2022, 17:00 WIB",
                        style: TextStyle(
                          color: Color(0xffff0000),
                          fontSize: 9,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(
                        width: 50,
                      ),
                      SizedBox(
                        width: 50,
                      ),
                      PopupMenuButton(
                        child: Icon(Icons.more_horiz_rounded),
                        onSelected: (value) {},
                        itemBuilder: (context) {
                          return [
                            PopupMenuItem(
                                value: 'Batalkan Pemesanan',
                                child: Row(
                                  children: [
                                    Icon(Icons.delete_forever),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 4.0),
                                      child: TextButton(
                                          onPressed: (() {
                                            showDialog(
                                                context: context,
                                                builder: (context) =>
                                                    AlertDialog(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                              borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                          16)),
                                                      content: Builder(
                                                          builder: (context) {
                                                        return Container(
                                                          width: 328,
                                                          height: 122,
                                                          child: Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceAround,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            children: [
                                                              SizedBox(
                                                                  height: 20),
                                                              Text(
                                                                "Yakin ingin batalkan Transaksi?",
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize: 15,
                                                                  fontFamily:
                                                                      "Inter",
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .w700,
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                  height: 20),
                                                              Text(
                                                                "Konfirmasi pembatalan transaksi",
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                      .black,
                                                                  fontSize: 10,
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                  height: 20),
                                                              Row(
                                                                children: [
                                                                  Container(
                                                                    width: 129,
                                                                    height: 32,
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              8),
                                                                      color: Color(
                                                                          0xfff0f0f0),
                                                                    ),
                                                                    child: Text(
                                                                      "Kembali",
                                                                      style:
                                                                          TextStyle(
                                                                        color: Color(
                                                                            0xb2000000),
                                                                        fontSize:
                                                                            10,
                                                                        fontFamily:
                                                                            "Inter",
                                                                        fontWeight:
                                                                            FontWeight.w600,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  Container(
                                                                    width: 129,
                                                                    height: 32,
                                                                    decoration:
                                                                        BoxDecoration(
                                                                      borderRadius:
                                                                          BorderRadius.circular(
                                                                              8),
                                                                      color: Color(
                                                                          0xff03ac0e),
                                                                    ),
                                                                    child: Text(
                                                                      "Kembali",
                                                                      style:
                                                                          TextStyle(
                                                                        color: Color(
                                                                            0xb2000000),
                                                                        fontSize:
                                                                            10,
                                                                        fontFamily:
                                                                            "Inter",
                                                                        fontWeight:
                                                                            FontWeight.w600,
                                                                      ),
                                                                    ),
                                                                  )
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                        );
                                                      }),
                                                    ));
                                          }),
                                          child: Text(
                                            "Batalkan Pemesanan",
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 13,
                                            ),
                                          )),
                                      // child: Text(
                                      //   "Batalkan Pemesanan",
                                      //   style: TextStyle(
                                      //     color: Colors.black,
                                      //     fontSize: 13,
                                      //   ),
                                      // ),
                                    ),
                                  ],
                                )),
                          ];
                        },
                      )
                    ],
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 10.0, right: 10, top: 8),
                    child: Stack(
                      children: [
                        Container(
                          width: 307,
                          height: 107.30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0x3f000000),
                                blurRadius: 4,
                                offset: Offset(0, 0),
                              ),
                            ],
                            color: Colors.white,
                          ),
                          child: Padding(
                            padding: const EdgeInsets.only(top: 8.0, left: 10),
                            child: Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          margin:
                                              const EdgeInsets.only(right: 12),
                                          width: 60,
                                          height: 60,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(6),
                                              color: Colors.blueGrey),
                                          child: FlutterLogo(size: 60),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Tiket Gunung",
                                              style: TextStyle(
                                                color: Color(0x7f000000),
                                                fontSize: 10,
                                                fontFamily: "Inter",
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                            Text(
                                              "Gunung Rinjani via Senaru  ",
                                              style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 15,
                                                fontFamily: "Inter",
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 4.0),
                                      child: Text(
                                        "Total Pembayaran",
                                        style: TextStyle(
                                          color: Color(0x7f000000),
                                          fontSize: 10,
                                          fontFamily: "Inter",
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      // mainAxisSize: MainAxisSize.max,
                                      children: [
                                        Text(
                                          "Rp20.000",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12,
                                            fontFamily: "Inter",
                                            fontWeight: FontWeight.w500,
                                          ),
                                        ),
                                        SizedBox(
                                          width: 60,
                                        ),
                                        SizedBox(
                                          width: 50,
                                        ),
                                        Text(
                                          "Lihat cara pembayaran",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                            color: Color(0x7f000000),
                                            fontSize: 12,
                                            fontFamily: "Inter",
                                            fontWeight: FontWeight.w500,
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                                // Padding(
                                //   padding: const EdgeInsets.only(left: 12.0),
                                //   child: Column(
                                //     crossAxisAlignment:
                                //         CrossAxisAlignment.start,
                                //     children: [
                                // Text(
                                //   "Tiket Gunung",
                                //   style: TextStyle(
                                //     color: Color(0x7f000000),
                                //     fontSize: 10,
                                //     fontFamily: "Inter",
                                //     fontWeight: FontWeight.w500,
                                //   ),
                                // ),
                                // Text(
                                //   "Gunung Rinjani via Senaru  ",
                                //   style: TextStyle(
                                //     color: Colors.black,
                                //     fontSize: 15,
                                //     fontFamily: "Inter",
                                //     fontWeight: FontWeight.w600,
                                //   ),
                                // ),
                                //     ],
                                //   ),
                                // )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
