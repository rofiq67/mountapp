import 'package:flutter/material.dart';
import 'package:mountapp/core/components/bg_appbar.dart';

class PembayaranTiket extends StatefulWidget {
  const PembayaranTiket({Key? key}) : super(key: key);

  @override
  State<PembayaranTiket> createState() => _PembayaranTiketState();
}

class _PembayaranTiketState extends State<PembayaranTiket> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        BgAppbar(judulAppbar: "Pembayaran", iconAct: null),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 91.0, horizontal: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: double.maxFinite,
                height: 165,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  boxShadow: const [
                    BoxShadow(
                      color: Color(0x3f000000),
                      blurRadius: 4,
                      offset: Offset(0, 2),
                    ),
                  ],
                  color: Colors.white,
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      vertical: 20.0, horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Gunung",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0x7f000000),
                          fontSize: 12,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      const Text(
                        "Gunung Merbabu via Selo",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              "Kode Pembayaran",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Color(0x7f000000),
                                fontSize: 12,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            Container(
                              width: 48,
                              height: 16,
                              color: Colors.amber,
                            )
                          ],
                        ),
                      ),
                      const Text(
                        "GN-12379H9DF89",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(top: 8.0),
                        child: Text(
                          "Jumlah Pembayaran",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: Color(0x7f000000),
                            fontSize: 12,
                            fontFamily: "Inter",
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: const [
                          Text(
                            "Rp. 123.456",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 15,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          // TextButton(
                          //   // onPressed: () {
                          //   //   showModalBottomSheet(
                          //   //       context: context,
                          //   //       shape: ,
                          //   //       builder: (context) {
                          //   //         return Container();
                          //   //       });
                          //   // },
                          // child:
                          Text(
                            "Lihat Detail",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Color(0x7f000000),
                              fontSize: 12,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                          // )
                        ],
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    SizedBox(
                      width: 50,
                    ),
                    Text(
                      "Lihat cara pembayaran",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Color(0x7f000000),
                        fontSize: 12,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ]),
    );
  }
}
