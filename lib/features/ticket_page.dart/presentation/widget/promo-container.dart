import 'package:flutter/material.dart';

class PromoContainer extends StatelessWidget {
  final String judulPromo;
  final String nilaiPromo;
  final syaratPromo;
  final syaratPromoBold;
  final String waktuPromo;
  const PromoContainer(
      {required this.judulPromo,
      required this.nilaiPromo,
      required this.syaratPromo,
      required this.syaratPromoBold,
      required this.waktuPromo,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
      ),
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            backgroundColor: Colors.white,
            side: const BorderSide(color: Colors.black54),
            elevation: 0,
          ),
          onPressed: () {
            print("aktip");
          },
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Text(
                      judulPromo,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Text(
                      nilaiPromo.toString(),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Color(0x7f000000),
                        fontSize: 10,
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0),
                  child: Row(
                    children: [
                      Icon(
                        Icons.info_outline,
                        color: Color(0x7f000000),
                        size: 14,
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(
                        syaratPromo,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0x7f000000),
                          fontSize: 10,
                        ),
                      ),
                      Text(syaratPromoBold,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color(0x7f000000),
                              fontSize: 10,
                              fontWeight: FontWeight.w700))
                    ],
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Row(
                  children: [
                    Icon(
                      Icons.timer_outlined,
                      color: Color(0x7f000000),
                      size: 14,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 4.0),
                      child: Text(
                        "Berakhir $waktuPromo jam lagi!",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Color(0x7f000000),
                          fontSize: 10,
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          )),
    );
  }
}
