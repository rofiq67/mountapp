import 'package:flutter/material.dart';

class TileCaraBayar extends StatelessWidget {
  final String metodeBayar;
  final int noDesk;
  final String deskBayarReguler;
  final String deskBayarBold;
  const TileCaraBayar(
      {required this.metodeBayar,
      required this.noDesk,
      required this.deskBayarReguler,
      required this.deskBayarBold,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ExpansionTile(
      title: Text(
        metodeBayar,
        style: TextStyle(
          color: Colors.black,
          fontSize: 15,
          fontFamily: "Inter",
          fontWeight: FontWeight.w700,
        ),
      ),
      children: [
        Container(
          color: Colors.black12,
          padding: const EdgeInsets.all(20),
          width: double.infinity,
          child: Column(children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 2.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    noDesk.toString(),
                    style: TextStyle(
                        fontSize: 12,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w400),
                  ),
                  RichText(
                      text: TextSpan(
                          text: deskBayarReguler,
                          style: TextStyle(
                              fontSize: 12,
                              color: Colors.black,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w400),
                          children: <TextSpan>[
                        TextSpan(
                            text: deskBayarBold,
                            style: TextStyle(
                                fontSize: 12,
                                color: Colors.black,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w700))
                      ])),
                ],
              ),
            ),
          ]),
        )
      ],
    );
  }
}
