import 'package:flutter/material.dart';

class GaleriView extends StatefulWidget {
  const GaleriView({Key? key}) : super(key: key);

  @override
  State<GaleriView> createState() => _GaleriViewState();
}

class _GaleriViewState extends State<GaleriView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Galeri",
          style: TextStyle(
            color: Colors.black,
            fontSize: 20,
            fontFamily: "Inter",
            fontWeight: FontWeight.w700,
          ),
        ),
        centerTitle: true,
        leading: Icon(
          Icons.arrow_back_ios_new_rounded,
          color: Colors.black,
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16),
            child: Row(
              children: [
                Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 4, vertical: 4),
                      width: 172,
                      height: 172,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: Colors.amber),
                      child: FlutterLogo(size: 162),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 4, vertical: 4),
                      width: 172,
                      height: 172,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: Colors.orange),
                      child: FlutterLogo(size: 162),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 4, vertical: 4),
                      width: 172,
                      height: 105,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: Colors.red),
                      child: FlutterLogo(size: 162),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 4, vertical: 4),
                      width: 172,
                      height: 172,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: Colors.blue),
                      child: FlutterLogo(size: 162),
                    )
                  ],
                ),
                Column(
                  children: [
                    Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 4, vertical: 4),
                      width: 172,
                      height: 302,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: Colors.green),
                      child: FlutterLogo(size: 162),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 4, vertical: 4),
                      width: 172,
                      height: 172,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: Colors.blue),
                      child: FlutterLogo(size: 162),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(
                          horizontal: 4, vertical: 4),
                      width: 172,
                      height: 172,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: Colors.blue),
                      child: FlutterLogo(size: 162),
                    )
                  ],
                )
              ],
            )),
      ),
    );
  }
}
