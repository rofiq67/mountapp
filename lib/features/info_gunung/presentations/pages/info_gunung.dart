import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:mountapp/features/info_gunung/presentations/widgets/card-cuaca.dart';
import 'package:mountapp/features/info_gunung/presentations/widgets/card-info-lainnya.dart';
import 'package:readmore/readmore.dart';

class InfoGunung extends StatefulWidget {
  const InfoGunung({Key? key}) : super(key: key);

  @override
  State<InfoGunung> createState() => _InfoGunungState();
}

class _InfoGunungState extends State<InfoGunung> {
  static final List<String> imgSlider = [
    'gunung_sumeru.png',
    'gunungapa.png',
    'gunung_merbabu.png'
    // 'https://berita.99.co/wp-content/uploads/2022/03/gunung-tertinggi-di-indonesia.jpg',
    // 'https://s3.theasianparent.com/tap-assets-prod/wp-c…ites/24/2021/12/pexels-archie-binamira-913215.jpg',
    // 'https://indonesiatraveler.id/wp-content/uploads/2020/06/Gunung-Prau1.jpg',
    // 'https://img.inews.co.id/media/822/files/inews_new/2022/04/22/daftar_gunung_di_atas_2500_mdpl.jpg'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.maxFinite,
          color: Colors.white70,
          height: 2000,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [
                  Container(
                      height: 335,
                      width: double.maxFinite,
                      color: Colors.grey.shade400,
                      child: Container(
                        height: 335,
                        // width: 36,
                        child: ListView(
                          scrollDirection: Axis.horizontal,
                          children: [
                            Image.asset(
                              'assets/images/gunungapa.png',
                              fit: BoxFit.fitWidth,
                            ),
                            Image.asset(
                              'assets/images/gunung_sumeru.png',
                              fit: BoxFit.cover,
                            ),
                            Image.asset(
                              'assets/images/gunung_merbabu.png',
                              fit: BoxFit.cover,
                            ),
                          ],
                        ),
                      )

                      // CarouselSlider(
                      //   options: CarouselOptions(
                      //     aspectRatio: 361 / 335,
                      //   ),
                      //   items: imgSlider
                      //       .map((item) => Image.asset(
                      //             'assets/images/${item}',
                      //             width: double.maxFinite,
                      //             height: 335,
                      //             fit: BoxFit.fill,
                      //             // height: 500,
                      //           ))
                      //       .toList(),
                      // ),

                      // Image.asset(
                      //   'assets/images/gunung_sumeru.png',
                      //   fit: BoxFit.fitWidth,
                      // ),
                      ),
                  AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    leading: Container(
                        margin: const EdgeInsets.only(left: 16),
                        width: 28,
                        height: 28,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                        ),
                        child: Icon(
                          Icons.arrow_back_ios_rounded,
                          color: Colors.black,
                        )),
                    actions: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16.0),
                        child: Container(
                          width: 38,
                          height: 38,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                          ),
                          child: Icon(
                            Icons.share,
                            color: Colors.black,
                            size: 18,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: Text(
                        "Rp10.000/tiket",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 20,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Row(
                        children: [
                          Container(
                            width: 18,
                            height: 10,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(2),
                              color: Color(0x60ff0000),
                            ),
                            child: Center(
                              child: Text(
                                "70%",
                                style: TextStyle(
                                  color: Color(0xffff0000),
                                  fontSize: 7,
                                  fontFamily: "Inter",
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4.0),
                            child: Stack(
                              children: [
                                Text(
                                  "Rp15.000",
                                  style: TextStyle(
                                    decoration: TextDecoration.lineThrough,
                                    color: Color(0x7f000000),
                                    fontSize: 8,
                                    fontFamily: "Inter",
                                    fontWeight: FontWeight.w600,
                                  ),
                                ),
                                // Container(
                                //   // alignment: Alignment.bottomCenter,
                                //   width: 36.50,
                                //   height: 0.50,
                                //   decoration: BoxDecoration(
                                //     border: Border.all(
                                //       color: Color(0xb2000000),
                                //       // width: -0.50,
                                //     ),
                                //   ),
                                // )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Text(
                        "Gunung Merbabu",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 6.0),
                      child: ReadMoreText(
                        'Gunung Merbabu merupakan salah satu gunung yang banyak diminati para pendaki, terletak di pulau jawa dengan ketinggian 3145 m membuat gunung ',
                        trimLines: 2,
                        // colorClickableText: ,
                        trimMode: TrimMode.Line,
                        trimCollapsedText: 'Baca selengkapnya',
                        moreStyle: TextStyle(
                          color: Color(0xff03ac0e),
                          fontSize: 13,
                        ),
                        trimExpandedText: '...Expand',
                        lessStyle: TextStyle(color: Color(0xff03ac0e)),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: Text(
                        "Perkiraan Cuaca",
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                          fontFamily: "Inter",
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    //cuaca
                    Container(
                      height: 130,
                      child:
                          ListView(scrollDirection: Axis.horizontal, children: [
                        CardCuaca(
                          hariCuaca: "Senin",
                          gambarCuaca: 'assets/images/awan-cerah.png',
                          suhuCuacaMin: 25,
                          suhuCuacaMax: 31,
                        ),
                        CardCuaca(
                          hariCuaca: "Selasa",
                          gambarCuaca: 'assets/images/awan-matahari.png',
                          suhuCuacaMin: 29,
                          suhuCuacaMax: 33,
                        ),
                        CardCuaca(
                          hariCuaca: "Rabu",
                          gambarCuaca: 'assets/images/awan-cerah.png',
                          suhuCuacaMin: 25,
                          suhuCuacaMax: 31,
                        ),
                        CardCuaca(
                          hariCuaca: "Kamis",
                          gambarCuaca: 'assets/images/awan-hujan.png',
                          suhuCuacaMin: 25,
                          suhuCuacaMax: 29,
                        ),
                        CardCuaca(
                          hariCuaca: "Jumat",
                          gambarCuaca: 'assets/images/awan-petir.png',
                          suhuCuacaMin: 24,
                          suhuCuacaMax: 28,
                        ),
                        CardCuaca(
                          hariCuaca: "sabtu",
                          gambarCuaca: 'assets/images/awan-hujan.png',
                          suhuCuacaMin: 25,
                          suhuCuacaMax: 29,
                        ),
                        CardCuaca(
                          hariCuaca: "Minggu",
                          gambarCuaca: 'assets/images/awan-cerah.png',
                          suhuCuacaMin: 25,
                          suhuCuacaMax: 31,
                        ),
                      ]),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    //rute
                    Text(
                      "Rute Pendakian",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      // margin: const EdgeInsets.only(top: 8),
                      width: double.maxFinite,
                      height: 102,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(16),
                        border: Border.all(
                          color: Colors.black,
                          width: 2,
                        ),
                        color: Color(0xff2a2a2a),
                      ),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    //galeri
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Galeri",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontFamily: "Inter",
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        // SizedBox(
                        //   width: 20,
                        // ),
                        Row(
                          children: [
                            Text(
                              "View",
                              style: TextStyle(
                                color: Color(0x7f000000),
                                fontSize: 10,
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            width: 190,
                            height: 300,
                            decoration: BoxDecoration(
                              color: Colors.amber,
                              borderRadius: BorderRadius.circular(4),
                            ),
                            child: Image.asset(
                              'assets/images/gunungapa.png',
                              fit: BoxFit.cover,
                            )),
                        // SizedBox(
                        //   width: 6,
                        // ),
                        Column(
                          children: [
                            Container(
                                width: 150,
                                height: 150,
                                decoration: BoxDecoration(
                                  color: Colors.blueAccent,
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                child: Image.asset(
                                  'assets/images/gunungapa.png',
                                  fit: BoxFit.cover,
                                )),
                            SizedBox(
                              height: 6,
                            ),
                            Container(
                                width: 150,
                                height: 150,
                                decoration: BoxDecoration(
                                  color: Colors.green,
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                child: Image.asset(
                                  'assets/images/gunungapa.png',
                                  fit: BoxFit.cover,
                                ))
                          ],
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 16,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 16),
                child: Text(
                  "Info Lainnya",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ExpansionTile(
                    collapsedBackgroundColor: Colors.white,
                    leading: Icon(
                      Icons.calendar_view_day_outlined,
                      size: 19,
                    ),
                    title: Text(
                      'Info Penukaran Tiket',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 10,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    children: [
                      Container(
                        width: double.maxFinite,
                        height: 30,
                        color: Colors.black,
                      )
                    ],
                  ),
                  ExpansionTile(
                    collapsedBackgroundColor: Colors.white,
                    leading: Icon(
                      Icons.info_outline,
                      size: 19,
                    ),
                    title: Text(
                      'Syarat dan Ketentuan',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 10,
                        fontFamily: "Inter",
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    children: [
                      Container(
                        height: 80,
                        width: double.maxFinite,
                        color: Colors.red,
                      )
                    ],
                  )
                ],
              ),
              SizedBox(
                height: 16,
              ),
              Padding(
                padding: const EdgeInsets.only(
                  left: 16.0,
                ),
                child: Text(
                  "Mungkin Kamu Suka",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Container(
                height: 200,
                child: ListView(scrollDirection: Axis.horizontal, children: [
                  CardInfoLainnya(
                      judulCard: "Gunung Merbabu Via Selo",
                      tanggalCard: null,
                      hargaCard: 150000,
                      gambarCard: 'assets/images/gunung_merbabu.png'),
                  CardInfoLainnya(
                      judulCard: "Open Trip Gunung Sumeru",
                      tanggalCard: "17 Mei - 20 Mei 2023",
                      hargaCard: 150000,
                      gambarCard: 'assets/images/gunungapa.png'),
                  CardInfoLainnya(
                      judulCard: "Gunung Merbabu Via Selo",
                      tanggalCard: "null",
                      hargaCard: 150000,
                      gambarCard: 'assets/images/gunung_merbabu.png'),
                  CardInfoLainnya(
                      judulCard: "Open Trip Gunung Sumeru",
                      tanggalCard: "17 Mei - 20 Mei 2023",
                      hargaCard: 150000,
                      gambarCard: 'assets/images/gunungapa.png'),
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
