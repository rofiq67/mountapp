import 'package:flutter/material.dart';

class CardInfoLainnya extends StatelessWidget {
  final String judulCard;
  final tanggalCard;
  final int hargaCard;
  final String gambarCard;
  const CardInfoLainnya(
      {required this.judulCard,
      required this.tanggalCard,
      required this.hargaCard,
      required this.gambarCard,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: const EdgeInsets.only(left: 16.0, top: 8),
          width: 150,
          height: 156,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: Colors.white,
              boxShadow: [BoxShadow(blurRadius: 4, spreadRadius: -1)]),
          child: Padding(
            padding: const EdgeInsets.only(top: 78.0, left: 8, right: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text(
                  judulCard,
                  maxLines: 4,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 12,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Text(
                  tanggalCard.toString(),
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 8,
                  ),
                ),
                SizedBox(height: 8),
                Text(
                  "Rp$hargaCard",
                  style: TextStyle(
                    color: Color(0xffbb0505),
                    fontSize: 12,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 16.0, top: 8),
          width: 150,
          height: 74,
          child: ClipRRect(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(8), topRight: Radius.circular(8)),
            child: Image.asset(
              gambarCard,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ],
    );
  }
}
