import 'package:flutter/material.dart';

class CardCuaca extends StatelessWidget {
  final String hariCuaca;
  final String gambarCuaca;
  final int suhuCuacaMin;
  final int suhuCuacaMax;
  const CardCuaca(
      {required this.hariCuaca,
      required this.gambarCuaca,
      required this.suhuCuacaMin,
      required this.suhuCuacaMax,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 3),
        padding: const EdgeInsets.symmetric(vertical: 8),
        width: 105,
        height: 122,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(16),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black,
                blurRadius: 8.0,
                spreadRadius: -1.9,
              )
            ]),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(
              hariCuaca,
              style: TextStyle(
                color: Colors.black,
                fontSize: 13,
                fontFamily: "Inter",
                fontWeight: FontWeight.w600,
              ),
            ),
            Image.asset(
              gambarCuaca,
              width: 66,
              height: 55.77,
            ),
            Text(
              "$suhuCuacaMin - $suhuCuacaMax",
              style: TextStyle(
                color: Colors.black,
                fontSize: 13,
                fontFamily: "Inter",
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
