import 'package:flutter/material.dart';

class ChatPage extends StatelessWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(120),
        child: AppBar(
          leading: Container(
            width: 2,
            padding: const EdgeInsets.symmetric(horizontal: 14.0, vertical: 16),
            child: PopupMenuButton(
                elevation: 20,
                offset: Offset(14, 20),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(8.0),
                    bottomRight: Radius.circular(8.0),
                    topLeft: Radius.circular(8.0),
                    topRight: Radius.circular(8.0),
                  ),
                ),
                child: Text(
                  "Edit",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 16,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w700,
                  ),
                ),
                itemBuilder: (context) {
                  return [
                    PopupMenuItem(
                      child: Text(
                        "Dibaca semua",
                        style: TextStyle(
                          color: Color(0xb2000000),
                          fontSize: 12,
                        ),
                      ),

                      //  Container(
                      //   width: 134,
                      //   height: 72,
                      //   decoration: BoxDecoration(
                      //     borderRadius: BorderRadius.circular(6),
                      //     boxShadow: [
                      //       BoxShadow(
                      //         color: Color(0x3f000000),
                      //         blurRadius: 8,
                      //         offset: Offset(0, 0),
                      //       ),
                      //     ],
                      //     color: Colors.white,
                      //   ),
                      // ),
                    ),
                    PopupMenuItem(
                        child: Text(
                      "Hapus",
                      style: TextStyle(
                        color: Color(0xb2000000),
                        fontSize: 12,
                      ),
                    ))
                  ];
                }),
          ),
          elevation: 1,
          backgroundColor: Colors.white,
          // title: ,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(0),
            child: Container(
              margin: const EdgeInsets.only(bottom: 16),
              width: 365,
              height: 45,
              child: TextField(
                decoration: InputDecoration(
                  hintText: "Cari",
                  hintStyle: TextStyle(
                    color: Color(0x7f000000),
                    fontSize: 16,
                  ),
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 13, horizontal: 16),
                  fillColor: Color(0xffEAEAEA),
                  filled: true,
                  focusColor: Color(0xffF9F9F9),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                    borderSide: BorderSide.none,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                    borderSide: BorderSide(
                      color: Color(0xff03AC0E),
                      width: 2.0,
                    ),
                  ),
                  suffixIcon: Icon(
                    Icons.search_rounded,
                    // color: Color(0xffeaeaea),
                  ),
                  suffixIconColor: Colors.black45,
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)),
                ),
              ),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  boxShadow: [BoxShadow(blurRadius: 4, spreadRadius: -2)]),
            ),
          ),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //belom dibaca
          Container(
            padding: const EdgeInsets.only(top: 16.0, left: 16),
            width: double.maxFinite,
            height: 80,
            color: Color(0xfff2f2f2),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Ninu",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              "ID : 7357491",
                              style: TextStyle(
                                color: Color(0x7f000000),
                                fontSize: 12,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        width: 255,
                        child: Text(
                          "Jadi muncak gak Nu??",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Text(
                  "07.45 AM",
                  style: TextStyle(
                    color: Color(0x7f000000),
                    fontSize: 12,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w500,
                  ),
                )
              ],
            ),
          ),
          //sudah dibaca
          Container(
            padding: const EdgeInsets.only(top: 16.0, left: 16),
            width: double.maxFinite,
            height: 80,
            color: Colors.white,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Ninu",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              "ID : 7357491",
                              style: TextStyle(
                                color: Color(0x7f000000),
                                fontSize: 12,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        width: 255,
                        child: Text(
                          "Jadi muncak gak Nu??",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Text(
                  "2 Mei",
                  style: TextStyle(
                    color: Color(0x7f000000),
                    fontSize: 12,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w500,
                  ),
                )
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.only(top: 16.0, left: 16),
            width: double.maxFinite,
            height: 80,
            color: Colors.white,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CircleAvatar(),
                Padding(
                  padding: const EdgeInsets.only(left: 8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "Ninu",
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 14,
                              fontFamily: "Inter",
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 8.0),
                            child: Text(
                              "ID : 7357491",
                              style: TextStyle(
                                color: Color(0x7f000000),
                                fontSize: 12,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          )
                        ],
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 8),
                        width: 255,
                        child: Text(
                          "Jadi muncak gak Nu??",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 12,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Text(
                  "2 Mei",
                  style: TextStyle(
                    color: Color(0x7f000000),
                    fontSize: 12,
                    fontFamily: "Inter",
                    fontWeight: FontWeight.w500,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
