import 'package:flutter/material.dart';

class ArticleArguments {
  final int id;
  final String title;
  final String illustrationPath;
  final List<Color> colors;
  ArticleArguments(
      {this.id = 0,
      this.title = "",
      this.illustrationPath = "",
      this.colors = const [Colors.white, Colors.grey]});
}
