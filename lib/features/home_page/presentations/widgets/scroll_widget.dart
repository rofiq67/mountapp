import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mountapp/features/home_page/data/models/data_banner.dart';

class ScrollWidget extends StatefulWidget {
  final List<DataBanner> data;
  const ScrollWidget({Key? key, this.data = const []}) : super(key: key);

  @override
  State<ScrollWidget> createState() => _ScrollWidgetState();
}

class _ScrollWidgetState extends State<ScrollWidget> {
  late List<DataBanner> data;
  late int activeIndex = 0;
  @override
  void initState() {
    data = widget.data;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(right: 17),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              for (int i = 0; i < data.length; i++)
                Container(
                  width: activeIndex == i ? 15 : 6,
                  height: 6,
                  margin: const EdgeInsets.only(
                    left: 4,
                    right: 4,
                    top: 32,
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: const Color(0xFF03AC0E).withOpacity(0.5)),
                )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 17.0),
          child: Container(
            margin: const EdgeInsets.only(
              top: 13,
            ),
            height: 221,
            child: ListView.builder(
                itemCount: data.length,
                padding: const EdgeInsets.only(
                  left: 0,
                  right: 8,
                ),
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Container(
                    margin: const EdgeInsets.only(right: 16),
                    height: 221,
                    width: 328,
                    decoration: BoxDecoration(
                      color: Colors.white70,
                      borderRadius: BorderRadius.circular(12),
                    ),
                    child: Stack(
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: Image.network(
                            data[index].imageUrl ?? '',
                            height: 221,
                            width: 328,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                    margin: const EdgeInsets.only(
                                        top: 8, right: 13),
                                    child: Text(
                                      'Baca Selengkapnya',
                                      style: GoogleFonts.roboto(
                                          fontSize: 8,
                                          fontWeight: FontWeight.normal),
                                    )),
                              ],
                            ),
                            Container(
                              margin: const EdgeInsets.symmetric(
                                  horizontal: 18, vertical: 15),
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 18, vertical: 10),
                              decoration: BoxDecoration(
                                color: Colors.transparent,
                                borderRadius: BorderRadius.circular(6),
                                boxShadow: const [
                                  BoxShadow(
                                    blurStyle: BlurStyle.inner,
                                    color: Colors.black12,
                                  )
                                ],
                              ),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                        '${data[index].cityName}, ${data[index].provinceName}',
                                        style: GoogleFonts.inter(
                                            fontSize: 8,
                                            fontWeight: FontWeight.w400)),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Text("${data[index].mountName}",
                                            style: GoogleFonts.roboto(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w700)),
                                        Visibility(
                                          visible: (data[index].note ?? '')
                                              .isNotEmpty,
                                          child: Text(
                                              ", via ${data[index].note}",
                                              style: GoogleFonts.roboto(
                                                  fontSize: 8,
                                                  fontWeight: FontWeight.w400)),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        RatingBarIndicator(
                                          rating: data[index].starPoint ?? 0,
                                          itemBuilder: (context, index) =>
                                              const Icon(
                                            Icons.star,
                                            color: Colors.amber,
                                          ),
                                          itemCount: 5,
                                          itemSize: 14,
                                          unratedColor:
                                              Colors.amber.withAlpha(50),
                                          direction: Axis.horizontal,
                                        ),
                                        Padding(
                                            padding:
                                                const EdgeInsets.only(left: 10),
                                            child: Text(
                                              '4,9',
                                              style: GoogleFonts.inter(
                                                  fontSize: 8,
                                                  fontWeight: FontWeight.w400),
                                            ))
                                      ],
                                    ),
                                  ]),
                            ),
                          ],
                        )
                      ],
                    ),
                  );
                }),
          ),
        ),
      ],
    );
  }
}
