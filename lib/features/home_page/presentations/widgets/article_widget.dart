import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mountapp/features/home_page/domains/entities/articles_arguments.dart';
import 'package:mountapp/features/home_page/presentations/pages/detail_recomendation_page.dart';

class ArticleWidget extends StatelessWidget {
  final ArticleArguments data;
  const ArticleWidget(
    this.data, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: () => Navigator.push(
          context,
          MaterialPageRoute<void>(
            builder: (BuildContext context) => DetailRecomendationPage(data.id),
          ),
        ),
        child: Container(
          // margin: const EdgeInsets.only(right: 2),
          // width: double.maxFinite/2,
          height: 110,
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: AlignmentDirectional.bottomCenter,
                  colors: data.colors)),
          child: Stack(
            alignment: Alignment.bottomRight,
            children: [
              Visibility(
                  visible: data.illustrationPath.isNotEmpty,
                  child: Image.asset(
                    data.illustrationPath,
                    width: double.maxFinite / 2,
                    height: 83,
                    alignment: Alignment.bottomRight,
                  )),
              SizedBox(
                width: double.maxFinite,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.only(left: 17, top: 9),
                      child: Text(
                        'Baca Selengkapnya',
                        style: GoogleFonts.roboto(
                            fontSize: 8,
                            fontWeight: FontWeight.w400,
                            color: Colors.white),
                      ),
                    ),
                    Container(
                      width: double.maxFinite / 3,
                      padding: const EdgeInsets.only(left: 17, right: 68),
                      margin: const EdgeInsets.only(top: 35),
                      child: Text(
                        data.title,
                        maxLines: 3,
                        style: GoogleFonts.roboto(
                            fontSize: 14,
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
