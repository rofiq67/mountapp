import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mountapp/features/home_page/domains/entities/articles_arguments.dart';
import 'package:mountapp/features/home_page/presentations/widgets/article_widget.dart';

class ArticleSection extends StatelessWidget {
  final List<ArticleArguments> data;
  const ArticleSection(this.data, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding:
              const EdgeInsets.only(left: 17, right: 17, top: 16, bottom: 8),
          child: Row(
            children: [
              Text('Artikel',
                  style: GoogleFonts.roboto(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Colors.black)),
              const Spacer(),
              Text(
                'Lainnya',
                style: GoogleFonts.roboto(
                    fontSize: 12,
                    fontWeight: FontWeight.w400,
                    color: Colors.black54),
              )
            ],
          ),
        ),

        //banner
        Column(
          children: [
            for (int i = 0; i < data.length; i += 2)
              Padding(
                padding: const EdgeInsets.only(bottom: 4),
                child: Row(
                  children: [
                    ArticleWidget(
                      ArticleArguments(
                        title: data[i].title,
                        illustrationPath: data[i].illustrationPath,
                        colors: data[i].colors,
                      ),
                    ),
                    const SizedBox(width: 4),
                    ArticleWidget(
                      ArticleArguments(
                        title: data[i + 1].title,
                        illustrationPath: data[i + 1].illustrationPath,
                        colors: data[i + 1].colors,
                      ),
                    ),
                  ],
                ),
              ),
          ],
        )
      ],
    );
  }
}
