import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../data/models/data_banner2.dart';

class ScrollWidget2 extends StatefulWidget {
  final List<DataBanner2> data2;
  const ScrollWidget2({Key? key, required this.data2}) : super(key: key);

  @override
  _ScrollWidget2State createState() => _ScrollWidget2State();
}

class _ScrollWidget2State extends State<ScrollWidget2> {
  late int activeIndex = 0;

  late final List<DataBanner2> data2;

  @override
  void initState() {
    data2 = widget.data2;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        // const Padding(
        //   padding: EdgeInsets.only(top: 16, left:17, right:17),
        // ),
        Padding(
          padding: const EdgeInsets.only(left: 17, right: 17, top: 16),
          child: Row(
            children: [
              Text('Rekomendasi',
                  style: GoogleFonts.roboto(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: const Color(0xFF03AC0E))),
              const Spacer(),
              for (int i = 0; i < data2.length; i++)
                Container(
                  width: activeIndex == i ? 15 : 6,
                  height: 6,
                  margin: const EdgeInsets.only(
                    left: 4,
                    right: 4,
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: const Color(0xFF03AC0E).withOpacity(0.5)),
                )
            ],
          ),
        ),

        //banner2
        Container(
          padding: const EdgeInsets.only(left: 17),
          margin: const EdgeInsets.only(top: 8),
          height: 149,
          child: ListView.builder(
            itemCount: 4,
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.only(right: 8),
            itemBuilder: (context, index) {
              return Stack(children: [
                Container(
                  width: 241,
                  height: 149,
                  margin: const EdgeInsets.only(right: 16),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 6,
                          spreadRadius: 0,
                          color: Colors.black.withOpacity(0.3),
                        ),
                      ]),
                ),
                Container(
                  margin:
                      const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(12),
                    child: Image.network(
                      data2[index].imageUrl2 ?? '',
                      height: 140,
                      width: 226,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                    margin: const EdgeInsets.only(top: 88, left: 18),
                    width: 210,
                    height: 50,
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(6),
                        boxShadow: [
                          BoxShadow(
                            blurRadius: 4,
                            spreadRadius: 0,
                            color: Colors.black.withOpacity(0.2),
                          )
                        ]),
                    child: Column(children: [
                      const Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 12, vertical: 4)),
                      Row(children: [
                        Text(
                          '${data2[index].cityName2}, ${data2[index].provinceName2}, ${data2[index].note2}',
                          style: GoogleFonts.roboto(
                              fontSize: 8, fontWeight: FontWeight.w400),
                        ),
                      ]),
                      Row(children: [
                        Text('${data2[index].mountName2}',
                            style: GoogleFonts.roboto(
                                fontSize: 16, fontWeight: FontWeight.w700)),
                      ]),
                      Row(
                        children: [
                          // RatingBarIndicator(
                          //   rating: data2[index].starPoint2 ?? 0,
                          //   itemBuilder: (context, index) => const Icon(
                          //     Icons.star,
                          //     color: Colors.amber,
                          //   ),
                          //   itemCount: 5,
                          //   itemSize: 14,
                          //   unratedColor: Colors.amber.withAlpha(50),
                          //   direction: Axis.horizontal,
                          // ),
                          RatingBarIndicator(
                            rating: 2.75,
                            // ignore: prefer_const_constructors
                            itemBuilder: (context, index) => Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            itemCount: 5,
                            itemSize: 6.0,
                            direction: Axis.horizontal,
                          ),
                          Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                '4,9',
                                style: GoogleFonts.inter(
                                    fontSize: 4, fontWeight: FontWeight.w400),
                              ))
                        ],
                      ),
                    ])),
              ]);
            },
          ),
        )
      ],
    );
  }
}
