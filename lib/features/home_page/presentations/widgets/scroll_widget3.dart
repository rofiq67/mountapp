import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mountapp/features/home_page/data/models/data_banner3.dart';

class ScrollWidget3 extends StatefulWidget {
  const ScrollWidget3({Key? key, required this.data3}) : super(key: key);
  final List<DataBanner3> data3;

  @override
  _ScrollWidget3State createState() => _ScrollWidget3State();
}

class _ScrollWidget3State extends State<ScrollWidget3> {
  late int activeIndex = 0;
  late final List<DataBanner3> data3;

  @override
  void initState() {
    data3 = widget.data3;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 16, left: 17, right: 17),
          child: Row(
            children: [
              Text('Open Trip',
                  style: GoogleFonts.roboto(
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      color: Colors.black)),
              const Spacer(),
              for (int i = 0; i < data3.length; i++)
                Container(
                  width: activeIndex == i ? 15 : 6,
                  height: 6,
                  margin: const EdgeInsets.only(
                    left: 4,
                    right: 4,
                  ),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: const Color(0xFF03AC0E).withOpacity(0.5)),
                )
            ],
          ),
        ),

        //banner
        Container(
          margin: const EdgeInsets.only(top: 8),
          height: 194,
          child: ListView.builder(
            itemCount: data3.length,
            scrollDirection: Axis.horizontal,
            padding: const EdgeInsets.symmetric(horizontal: 17),
            itemBuilder: (context, index) {
              return Container(
                margin:
                    EdgeInsets.only(right: index < data3.length - 1 ? 16 : 0),
                height: 194,
                width: 193,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(6),
                  color: Colors.lightGreen,
                ),
                child: Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(12),
                      child: Image.network(
                        data3[index].imageUrl3 ?? '',
                        height: 194,
                        width: 193,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(
                        top: 134,
                        left: 7,
                      ),
                      width: 165.88,
                      height: 52,
                      decoration: BoxDecoration(
                          color: Colors.transparent,
                          borderRadius: BorderRadius.circular(6),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 4,
                              spreadRadius: 0,
                              color: Colors.black.withOpacity(0.2),
                            )
                          ]),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 9, vertical: 5),
                        child: Column(children: [
                          // const Padding(padding: EdgeInsets.symmetric(horizontal : 50, vertical: 4),),
                          Row(
                            children: [
                              Text(
                                'Open trip',
                                style: GoogleFonts.roboto(
                                    fontSize: 8, fontWeight: FontWeight.w400),
                              )
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                '${data3[index].mountName3}',
                                style: GoogleFonts.roboto(
                                    fontSize: 16, fontWeight: FontWeight.w700),
                              ),
                              Expanded(
                                child: Text('${data3[index].note3}',
                                    overflow: TextOverflow.ellipsis,
                                    style: GoogleFonts.roboto(
                                        fontSize: 8,
                                        fontWeight: FontWeight.w400)),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text('${data3[index].price3}',
                                  style: GoogleFonts.roboto(
                                      fontSize: 8,
                                      fontWeight: FontWeight.w600)),
                            ],
                          )
                        ]),
                      ),
                    )
                  ],
                ),
              );
            },
          ),
        )
      ],
    );
  }
}
