import 'package:flutter/material.dart';

class ListSearchPage2 extends StatelessWidget {
  const ListSearchPage2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: const Icon(
          Icons.arrow_back_ios_new_rounded,
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
        title: Container(
            width: 246,
            height: 32,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(6),
                color: const Color(0xFFF9F9F9),
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x3f000000),
                    blurRadius: 4,
                    offset: Offset(0, 0),
                  )
                ]),
            child: const Center(
              child: TextField(
                decoration: InputDecoration(
                    suffixIcon: Icon(
                      Icons.search,
                      color: Color(0x7f7f3a44),
                    ),
                    border: InputBorder.none),
              ),
            )),
        actions: <Widget>[
          IconButton(
              icon: const Icon(
                Icons.filter_list_rounded,
                color: Colors.black,
              ),
              onPressed: () {
                showModalBottomSheet(
                  context: context,
                  isScrollControlled: true,
                  builder: (BuildContext context) {
                    return SizedBox(
                      height: 895,
                      width: 360,
                      // color: Colors.red,
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 14.0),
                              child: Container(
                                width: 99,
                                height: 4,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: const Color(0xffdfdfdf),
                                    width: 3,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(top: 3.0, left: 16),
                            child: Text(
                              "Filter",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(top: 16.0, left: 16),
                            child: Text(
                              "Urutkan",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8, left: 16),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    ElevatedButton(
                                      onPressed: (() {}),
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor:
                                              const Color(0x4902ac0d),
                                          backgroundColor: Colors.white,
                                          elevation: 0,
                                          shape: RoundedRectangleBorder(
                                              side: const BorderSide(
                                                  color: Color(0x7f000000),
                                                  width: 1),
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                      child: const Text(
                                        "Rekomendasi",
                                        style: TextStyle(
                                          color: Color(0xb2000000),
                                          fontSize: 13,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: ElevatedButton(
                                        onPressed: (() {}),
                                        style: ElevatedButton.styleFrom(
                                            foregroundColor:
                                                const Color(0x4902ac0d),
                                            backgroundColor: Colors.white,
                                            elevation: 0,
                                            shape: RoundedRectangleBorder(
                                                side: const BorderSide(
                                                    color: Color(0x7f000000),
                                                    width: 1),
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        child: const Text(
                                          "Tertinggi",
                                          style: TextStyle(
                                            color: Color(0xb2000000),
                                            fontSize: 13,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: ElevatedButton(
                                        onPressed: (() {}),
                                        style: ElevatedButton.styleFrom(
                                            foregroundColor:
                                                const Color(0x4902ac0d),
                                            backgroundColor: Colors.white,
                                            elevation: 0,
                                            shape: RoundedRectangleBorder(
                                                side: const BorderSide(
                                                    color: Color(0x7f000000),
                                                    width: 1),
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        child: const Text(
                                          "Terendah",
                                          style: TextStyle(
                                            color: Color(0xb2000000),
                                            fontSize: 13,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    ElevatedButton(
                                      onPressed: (() {}),
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor:
                                              const Color(0x4902ac0d),
                                          backgroundColor: Colors.white,
                                          elevation: 0,
                                          shape: RoundedRectangleBorder(
                                              side: const BorderSide(
                                                  color: Color(0x7f000000),
                                                  width: 1),
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                      child: const Text(
                                        "Promo",
                                        style: TextStyle(
                                          color: Color(0xb2000000),
                                          fontSize: 13,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: ElevatedButton(
                                        onPressed: (() {}),
                                        style: ElevatedButton.styleFrom(
                                            foregroundColor:
                                                const Color(0x4902ac0d),
                                            backgroundColor: Colors.white,
                                            elevation: 0,
                                            shape: RoundedRectangleBorder(
                                                side: const BorderSide(
                                                    color: Color(0x7f000000),
                                                    width: 1),
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        child: const Text(
                                          "Harga Tertinggi",
                                          style: TextStyle(
                                            color: Color(0xb2000000),
                                            fontSize: 13,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: ElevatedButton(
                                        onPressed: (() {}),
                                        style: ElevatedButton.styleFrom(
                                            foregroundColor:
                                                const Color(0x4902ac0d),
                                            backgroundColor: Colors.white,
                                            elevation: 0,
                                            shape: RoundedRectangleBorder(
                                                side: const BorderSide(
                                                    color: Color(0x7f000000),
                                                    width: 1),
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        child: const Text(
                                          "Harga Terendah",
                                          style: TextStyle(
                                            color: Color(0xb2000000),
                                            fontSize: 13,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    ElevatedButton(
                                      onPressed: (() {}),
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor:
                                              const Color(0x4902ac0d),
                                          backgroundColor: Colors.white,
                                          elevation: 0,
                                          shape: RoundedRectangleBorder(
                                              side: const BorderSide(
                                                  color: Color(0x7f000000),
                                                  width: 1),
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                      child: const Text(
                                        "Paling Banyak Dikunjungi",
                                        style: TextStyle(
                                          color: Color(0xb2000000),
                                          fontSize: 13,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: ElevatedButton(
                                        onPressed: (() {}),
                                        style: ElevatedButton.styleFrom(
                                            foregroundColor:
                                                const Color(0x4902ac0d),
                                            backgroundColor: Colors.white,
                                            elevation: 0,
                                            shape: RoundedRectangleBorder(
                                                side: const BorderSide(
                                                    color: Color(0x7f000000),
                                                    width: 1),
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        child: const Text(
                                          "Terpopuler",
                                          style: TextStyle(
                                            color: Color(0xb2000000),
                                            fontSize: 13,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: const [
                              Padding(
                                padding: EdgeInsets.only(top: 16.0, left: 16),
                                child: Text(
                                  "Lokasi",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 15,
                                    fontFamily: "Inter",
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: EdgeInsets.only(right: 21.0),
                                child: Text(
                                  "Lainnya",
                                  style: TextStyle(
                                    color: Color(0xff03ac0e),
                                    fontSize: 13,
                                    fontFamily: "Inter",
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              )
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8, left: 16),
                            child: Column(
                              children: [
                                Row(
                                  children: [
                                    ElevatedButton(
                                      onPressed: (() {}),
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor:
                                              const Color(0x4902ac0d),
                                          backgroundColor: Colors.white,
                                          elevation: 0,
                                          shape: RoundedRectangleBorder(
                                              side: const BorderSide(
                                                  color: Color(0x7f000000),
                                                  width: 1),
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                      child: const Text(
                                        "Jawa Tengah",
                                        style: TextStyle(
                                          color: Color(0xb2000000),
                                          fontSize: 13,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: ElevatedButton(
                                        onPressed: (() {}),
                                        style: ElevatedButton.styleFrom(
                                            foregroundColor:
                                                const Color(0x4902ac0d),
                                            backgroundColor: Colors.white,
                                            elevation: 0,
                                            shape: RoundedRectangleBorder(
                                                side: const BorderSide(
                                                    color: Color(0x7f000000),
                                                    width: 1),
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        child: const Text(
                                          "Jawa Timur",
                                          style: TextStyle(
                                            color: Color(0xb2000000),
                                            fontSize: 13,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: ElevatedButton(
                                        onPressed: (() {}),
                                        style: ElevatedButton.styleFrom(
                                            foregroundColor:
                                                const Color(0x4902ac0d),
                                            backgroundColor: Colors.white,
                                            elevation: 0,
                                            shape: RoundedRectangleBorder(
                                                side: const BorderSide(
                                                    color: Color(0x7f000000),
                                                    width: 1),
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        child: const Text(
                                          "Jawa Barat",
                                          style: TextStyle(
                                            color: Color(0xb2000000),
                                            fontSize: 13,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    ElevatedButton(
                                      onPressed: (() {}),
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor:
                                              const Color(0x4902ac0d),
                                          backgroundColor: Colors.white,
                                          elevation: 0,
                                          shape: RoundedRectangleBorder(
                                              side: const BorderSide(
                                                  color: Color(0x7f000000),
                                                  width: 1),
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                      child: const Text(
                                        "Banten",
                                        style: TextStyle(
                                          color: Color(0xb2000000),
                                          fontSize: 13,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: ElevatedButton(
                                        onPressed: (() {}),
                                        style: ElevatedButton.styleFrom(
                                            foregroundColor:
                                                const Color(0x4902ac0d),
                                            backgroundColor: Colors.white,
                                            elevation: 0,
                                            shape: RoundedRectangleBorder(
                                                side: const BorderSide(
                                                    color: Color(0x7f000000),
                                                    width: 1),
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        child: const Text(
                                          "Nusa Dua",
                                          style: TextStyle(
                                            color: Color(0xb2000000),
                                            fontSize: 13,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: ElevatedButton(
                                        onPressed: (() {}),
                                        style: ElevatedButton.styleFrom(
                                            foregroundColor:
                                                const Color(0x4902ac0d),
                                            backgroundColor: Colors.white,
                                            elevation: 0,
                                            shape: RoundedRectangleBorder(
                                                side: const BorderSide(
                                                    color: Color(0x7f000000),
                                                    width: 1),
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        child: const Text(
                                          "Kalimatan Timur",
                                          style: TextStyle(
                                            color: Color(0xb2000000),
                                            fontSize: 13,
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    ElevatedButton(
                                      onPressed: (() {}),
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor:
                                              const Color(0x4902ac0d),
                                          backgroundColor: Colors.white,
                                          elevation: 0,
                                          shape: RoundedRectangleBorder(
                                              side: const BorderSide(
                                                  color: Color(0x7f000000),
                                                  width: 1),
                                              borderRadius:
                                                  BorderRadius.circular(8))),
                                      child: const Text(
                                        "Kalimatan Barat",
                                        style: TextStyle(
                                          color: Color(0xb2000000),
                                          fontSize: 13,
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8),
                                      child: ElevatedButton(
                                        onPressed: (() {}),
                                        style: ElevatedButton.styleFrom(
                                            foregroundColor:
                                                const Color(0x4902ac0d),
                                            backgroundColor: Colors.white,
                                            elevation: 0,
                                            shape: RoundedRectangleBorder(
                                                side: const BorderSide(
                                                    color: Color(0x7f000000),
                                                    width: 1),
                                                borderRadius:
                                                    BorderRadius.circular(8))),
                                        child: const Text(
                                          "Sumatera",
                                          style: TextStyle(
                                            color: Color(0xb2000000),
                                            fontSize: 13,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(top: 16.0, left: 16),
                            child: Text(
                              "Tersedia",
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontFamily: "Inter",
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          Padding(
                              padding: const EdgeInsets.only(top: 8, left: 16),
                              child: Column(children: [
                                Row(children: [
                                  ElevatedButton(
                                    onPressed: (() {}),
                                    style: ElevatedButton.styleFrom(
                                        foregroundColor:
                                            const Color(0x4902ac0d),
                                        backgroundColor: Colors.white,
                                        elevation: 0,
                                        shape: RoundedRectangleBorder(
                                            side: const BorderSide(
                                                color: Color(0x7f000000),
                                                width: 1),
                                            borderRadius:
                                                BorderRadius.circular(8))),
                                    child: const Text(
                                      "Siap Dipesan",
                                      style: TextStyle(
                                        color: Color(0xb2000000),
                                        fontSize: 13,
                                      ),
                                    ),
                                  ),
                                ])
                              ])),
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 169, left: 6),
                              child: Container(
                                width: 326,
                                height: 50,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                  color: const Color(0xff03ac0e),
                                ),
                                child: const Center(
                                  child: Text(
                                    "Simpan",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 14,
                                      fontFamily: "Inter",
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  },
                );
              })
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(17.0),
        child: Center(
          child: Column(
            children: [
              Container(
                width: 328,
                height: 149,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: Colors.white,
                    boxShadow: const [
                      BoxShadow(
                        color: Color(0x3f000000),
                        blurRadius: 4,
                        offset: Offset(0, 0),
                      )
                    ]),
                child: Stack(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 7, vertical: 6),
                      child: Row(
                        children: [
                          Column(
                            children: [
                              Container(
                                width: 137,
                                height: 137,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(6),
                                  // color: Colors.blueAccent,
                                ),
                                child: Image.asset(
                                  'assets/images/gunung_merbabu.png',
                                  fit: BoxFit.cover,
                                ),
                              )
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 6.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Container(
                                      width: 49,
                                      height: 13.43,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(6),
                                        color: const Color(0xd8ff5656),
                                      ),
                                      child: const Center(
                                        child: Text(
                                          "Terpopuler",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              fontSize: 6,
                                              fontWeight: FontWeight.w500,
                                              color: Colors.white),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(left: 6),
                                      child: Container(
                                          width: 30,
                                          height: 13.43,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(6),
                                            color: const Color(0x7fffce31),
                                          ),
                                          child: Row(
                                            children: const [
                                              Padding(
                                                padding: EdgeInsets.all(2),
                                                child: Icon(
                                                  Icons.star,
                                                  size: 11,
                                                  color: Color(0xffffce31),
                                                ),
                                              ),
                                              Text(
                                                "4,5",
                                                style: TextStyle(
                                                  color: Color(0xffffce31),
                                                  fontSize: 8,
                                                ),
                                              )
                                            ],
                                          )),
                                    )
                                  ],
                                ),
                                const Padding(
                                  padding: EdgeInsets.only(top: 19),
                                  child: Text(
                                    "12345 Mdpl",
                                    style: TextStyle(
                                      fontSize: 8,
                                    ),
                                  ),
                                ),
                                Padding(
                                    padding: const EdgeInsets.only(top: 4),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: const [
                                        Text("Gunung Merbabu,",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w700)),
                                        Text("via Selo",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w600,
                                                fontSize: 6))
                                      ],
                                    )),
                                const Padding(
                                    padding: EdgeInsets.only(top: 6),
                                    child: Text("Lombok, NTT",
                                        style: TextStyle(
                                            fontSize: 8,
                                            fontWeight: FontWeight.w400))),
                                Padding(
                                  padding: const EdgeInsets.only(top: 14.0),
                                  child: Row(
                                    children: [
                                      Container(
                                        width: 18,
                                        height: 10,
                                        decoration: BoxDecoration(
                                            color: const Color(0x60ff0000),
                                            borderRadius:
                                                BorderRadius.circular(2)),
                                        child: const Center(
                                          child: Text("50%",
                                              style: TextStyle(
                                                color: Color(0xffff0000),
                                                fontSize: 7,
                                                fontFamily: "Inter",
                                                fontWeight: FontWeight.w600,
                                              )),
                                        ),
                                      ),
                                      // Stack(
                                      //   children: [
                                      //     const Text(
                                      //       "Rp10.000",
                                      //       style: TextStyle(
                                      //         color: Color(0x7f000000),
                                      //         fontSize: 8,
                                      //         fontFamily: "Inter",
                                      //         fontWeight: FontWeight.w600,
                                      //       ),
                                      //     ),
                                      //     Container(
                                      //       padding:
                                      //           const EdgeInsets.only(left: 4),
                                      //       width: 0.50,
                                      //       height: 0.50,
                                      //       decoration: BoxDecoration(
                                      //         border: Border.all(
                                      //           color: const Color(0xb2000000),
                                      //           width: -0.50,
                                      //         ),
                                      //       ),
                                      //     )
                                      //   ],
                                      // )
                                      const Padding(
                                        padding: EdgeInsets.only(left: 4.0),
                                        child: Text(
                                          "Rp10.000",
                                          style: TextStyle(
                                            color: Color(0x7f000000),
                                            fontSize: 8,
                                            fontFamily: "Inter",
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: Row(
                                    children: [
                                      const Text(
                                        "Rp5.000/tiket",
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 15,
                                          fontFamily: "Inter",
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 14.0),
                                        child: Container(
                                          width: 46,
                                          height: 9,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(2),
                                            color: const Color(0x6003ac0e),
                                          ),
                                          child: const Center(
                                            child: Text(
                                              "Kuota Tersisa = 20",
                                              style: TextStyle(
                                                color: Color(0x7f000000),
                                                fontSize: 5,
                                                fontFamily: "Inter",
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Container(
                  width: 328,
                  height: 149,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.white,
                      boxShadow: const [
                        BoxShadow(
                          color: Color(0x3f000000),
                          blurRadius: 4,
                          offset: Offset(0, 0),
                        )
                      ]),
                  child: Stack(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 7, vertical: 6),
                        child: Row(
                          children: [
                            Column(
                              children: [
                                Container(
                                  width: 137,
                                  height: 137,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(6),
                                    // color: Colors.blueAccent,
                                  ),
                                  child: Image.asset(
                                    'assets/images/gunung_merbabu.png',
                                    fit: BoxFit.cover,
                                  ),
                                )
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 6.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    children: [
                                      Container(
                                        width: 49,
                                        height: 13.43,
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(6),
                                          color: const Color(0xd8ff5656),
                                        ),
                                        child: const Center(
                                          child: Text(
                                            "Terpopuler",
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                fontSize: 6,
                                                fontWeight: FontWeight.w500,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(left: 6),
                                        child: Container(
                                            width: 30,
                                            height: 13.43,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(6),
                                              color: const Color(0x7fffce31),
                                            ),
                                            child: Row(
                                              children: const [
                                                Padding(
                                                  padding: EdgeInsets.all(2),
                                                  child: Icon(
                                                    Icons.star,
                                                    size: 11,
                                                    color: Color(0xffffce31),
                                                  ),
                                                ),
                                                Text(
                                                  "4,5",
                                                  style: TextStyle(
                                                    color: Color(0xffffce31),
                                                    fontSize: 8,
                                                  ),
                                                )
                                              ],
                                            )),
                                      )
                                    ],
                                  ),
                                  const Padding(
                                    padding: EdgeInsets.only(top: 19),
                                    child: Text(
                                      "12345 Mdpl",
                                      style: TextStyle(
                                        fontSize: 8,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                      padding: const EdgeInsets.only(top: 4),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: const [
                                          Text("Gunung Merbabu,",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w700)),
                                          Text("via Selo",
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 6))
                                        ],
                                      )),
                                  const Padding(
                                      padding: EdgeInsets.only(top: 6),
                                      child: Text("Lombok, NTT",
                                          style: TextStyle(
                                              fontSize: 8,
                                              fontWeight: FontWeight.w400))),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 14.0),
                                    child: Row(
                                      children: [
                                        Container(
                                          width: 18,
                                          height: 10,
                                          decoration: BoxDecoration(
                                              color: const Color(0x60ff0000),
                                              borderRadius:
                                                  BorderRadius.circular(2)),
                                          child: const Center(
                                            child: Text("50%",
                                                style: TextStyle(
                                                  color: Color(0xffff0000),
                                                  fontSize: 7,
                                                  fontFamily: "Inter",
                                                  fontWeight: FontWeight.w600,
                                                )),
                                          ),
                                        ),
                                        // Stack(
                                        //   children: [
                                        //     const Text(
                                        //       "Rp10.000",
                                        //       style: TextStyle(
                                        //         color: Color(0x7f000000),
                                        //         fontSize: 8,
                                        //         fontFamily: "Inter",
                                        //         fontWeight: FontWeight.w600,
                                        //       ),
                                        //     ),
                                        //     Container(
                                        //       padding:
                                        //           const EdgeInsets.only(left: 4),
                                        //       width: 0.50,
                                        //       height: 0.50,
                                        //       decoration: BoxDecoration(
                                        //         border: Border.all(
                                        //           color: const Color(0xb2000000),
                                        //           width: -0.50,
                                        //         ),
                                        //       ),
                                        //     )
                                        //   ],
                                        // )
                                        const Padding(
                                          padding: EdgeInsets.only(left: 4.0),
                                          child: Text(
                                            "Rp10.000",
                                            style: TextStyle(
                                              color: Color(0x7f000000),
                                              fontSize: 8,
                                              fontFamily: "Inter",
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: Row(
                                      children: [
                                        const Text(
                                          "Rp5.000/tiket",
                                          style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 15,
                                            fontFamily: "Inter",
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(left: 14.0),
                                          child: Container(
                                            width: 46,
                                            height: 9,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(2),
                                              color: const Color(0x6003ac0e),
                                            ),
                                            child: const Center(
                                              child: Text(
                                                "Kuota Tersisa = 20",
                                                style: TextStyle(
                                                  color: Color(0x7f000000),
                                                  fontSize: 5,
                                                  fontFamily: "Inter",
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
