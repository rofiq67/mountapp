import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mountapp/features/home_page/data/models/data_banner.dart';
import 'package:mountapp/features/home_page/data/models/data_banner3.dart';
import 'package:mountapp/features/home_page/domains/entities/articles_arguments.dart';
import 'package:mountapp/features/home_page/presentations/widgets/artikel_section.dart';
import 'package:mountapp/features/home_page/presentations/widgets/scroll_widget.dart';
import 'package:mountapp/features/home_page/presentations/widgets/scroll_widget3.dart';
import 'package:mountapp/features/home_page/presentations/widgets/scroll_widget4.dart';

// import '../../../../core/components/bottom_widget.dart';
import '../../data/models/data_banner2.dart';
import '../../data/models/data_banner4.dart';
import '../widgets/scroll_widget2.dart';

class HomePage extends StatelessWidget {
  final List<DataBanner> database = [
    DataBanner(
        cityName: "Solo",
        provinceName: "Jawa Tengah",
        mountName: "Merbabu",
        note: "Selo",
        starPoint: 3.5,
        imageUrl:
            "https://akcdn.detik.net.id/visual/2017/04/18/30656317-cedc-466c-8c84-a8b988da7f87_169.jpg?w=650"),
    DataBanner(
        cityName: "Wonosobo",
        provinceName: "Jawa Timur",
        mountName: "Merbabu",
        note: "Kopeng",
        starPoint: 3.5,
        imageUrl:
            "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2018/10/merbabu-.jpg"),
    DataBanner(
        cityName: "Magelang",
        provinceName: "Jawa Barat",
        mountName: "Merbabu",
        note: "",
        starPoint: 3.5,
        imageUrl:
            "https://inakoran.com/uploads/2020/02/03/1580730078-p855d11e922e26b819b7be65dfc04cdf2.jpg")
  ];

  final List<ArticleArguments> dataArticle = [
    ArticleArguments(
      id: 11,
      title: 'Tips Mendaki Bagi Pemula',
      illustrationPath: 'assets/images/mendaki_ils_artikel.png',
      colors: const [Color(0xFF46C76A), Color(0xFF4EEE7B)],
    ),
    ArticleArguments(
      id: 15,
      title: 'Pertolongan \nPertama \nSaat Mendaki',
      illustrationPath: "assets/images/pp_ils_artikel.png",
      colors: const [Color(0xFFB15234), Color(0xFFFF3D00)],
    ),
    ArticleArguments(
      id: 19,
      title: 'Rekomendasi\nGunung Bagi \nPemula',
      illustrationPath: 'assets/images/gunung_ils_artikel.png',
      colors: const [Color(0xFF478EAD), Color(0xFF3E98BE)],
    ),
    ArticleArguments(
      id: 70,
      title: 'Tips Solo \nHiking',
      illustrationPath: "assets/images/tips_daki_ils_artikel.png",
      colors: const [Color(0xFFAB47AD), Color(0xFFEB10F1)],
    ),
  ];

  //banner 2
  final List<DataBanner2> database2 = [
    DataBanner2(
        cityName2: "Solo",
        provinceName2: "Jawa Tengah",
        mountName2: "Merbabu",
        note2: "Selo",
        starPoint2: 3.5,
        imageUrl2:
            "https://akcdn.detik.net.id/visual/2017/04/18/30656317-cedc-466c-8c84-a8b988da7f87_169.jpg?w=650"),
    DataBanner2(
        cityName2: "Jakarta",
        provinceName2: "Jawa Tengah",
        mountName2: "Sindoro",
        note2: "Selo",
        starPoint2: 3.5,
        imageUrl2:
            "https://akcdn.detik.net.id/visual/2017/04/18/30656317-cedc-466c-8c84-a8b988da7f87_169.jpg?w=650"),
    DataBanner2(
        cityName2: "Surabaya",
        provinceName2: "Jawa Tengah",
        mountName2: "Merapi",
        note2: "Selo",
        starPoint2: 3.5,
        imageUrl2:
            "https://akcdn.detik.net.id/visual/2017/04/18/30656317-cedc-466c-8c84-a8b988da7f87_169.jpg?w=650"),
  ];
  //banner 3
  final List<DataBanner3> database3 = [
    DataBanner3(
      mountName3: "Gunung Gede",
      note3: ", via Putri",
      price3: "Rp15.000/org",
      imageUrl3:
          "https://media.suara.com/pictures/970x544/2021/09/01/95965-taman-nasional-gunung-gede-pangrango-tnggp.jpg",
    ),
    DataBanner3(
      mountName3: "Gunung Sindoro",
      note3: ", via Mana",
      price3: "Rp50.000/org",
      imageUrl3:
          "https://assets.tokko.io/a01b33ac-7965-41eb-8106-b472f29b962c/products/images/7868fab2-4bce-45ca-a6ab-02293eceeaba",
    ),
    DataBanner3(
      mountName3: "Gunung Bromo",
      note3: ", via Putri",
      price3: "Rp15.000/org",
      imageUrl3:
          "https://media.suara.com/pictures/970x544/2021/09/01/95965-taman-nasional-gunung-gede-pangrango-tnggp.jpg",
    ),
  ];
  final List<DataBanner4> database4 = [
    DataBanner4(
        cityName4: "Solo",
        provinceName4: "Jawa Tengah",
        mountName4: "Merbabu",
        note4: "Selo",
        starPoint4: 3.5,
        imageUrl4:
            "https://akcdn.detik.net.id/visual/2017/04/18/30656317-cedc-466c-8c84-a8b988da7f87_169.jpg?w=650"),
    DataBanner4(
        cityName4: "Jakarta",
        provinceName4: "Jawa Tengah",
        mountName4: "Sindoro",
        note4: "Selo",
        starPoint4: 3.5,
        imageUrl4:
            "https://akcdn.detik.net.id/visual/2017/04/18/30656317-cedc-466c-8c84-a8b988da7f87_169.jpg?w=650"),
    DataBanner4(
        cityName4: "Surabaya",
        provinceName4: "Jawa Tengah",
        mountName4: "Merapi",
        note4: "Selo",
        starPoint4: 3.5,
        imageUrl4:
            "https://akcdn.detik.net.id/visual/2017/04/18/30656317-cedc-466c-8c84-a8b988da7f87_169.jpg?w=650"),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // bottomNavigationBar: const BottomWidgett(),
      body: Stack(
        alignment: Alignment.topRight,
        children: [
          AspectRatio(
            aspectRatio: 5760 / 5104,
            child: Image.asset(
              'assets/images/bggunung.png',
              fit: BoxFit.fitWidth,
            ),
          ),
          SafeArea(
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.transparent,
                  shadowColor: Colors.transparent,
                ),
                onPressed: () {},
                child: const Icon(
                  Icons.notifications,
                  color: Colors.yellow,
                )),
          ),
          SingleChildScrollView(
            child: Container(
              width: double.maxFinite,
              height: 1500,
              margin: const EdgeInsets.only(
                top: 248,
              ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(25),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 17, top: 25),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Hai Olivia',
                                style: GoogleFonts.roboto(
                                    fontSize: 16, fontWeight: FontWeight.w400)),
                            Text('Mau mendaki kemana?',
                                style: GoogleFonts.roboto(
                                    fontSize: 20, fontWeight: FontWeight.w600))
                          ],
                        ),

                        // ElevatedButton(
                        //   onPressed: () {},
                        //   child: IconButton(
                        //     icon: Image.asset(
                        //       'assets/icons/icon_search.png',
                        //       width: 18,
                        //       height: 18,
                        //     ),
                        //     iconSize: 5,
                        //     onPressed: () {},
                        //   ),

                        //   // Color(0x03ac0e)

                        //   style: ElevatedButton.styleFrom(
                        //     shape: const CircleBorder(
                        //       side: BorderSide.none,
                        //     ),
                        //     primary: Colors.white,
                        //     shadowColor: Colors.black26,
                        //   ),
                        // ),
                        // //  Icon(Icons.search)
                      ],
                    ),
                  ),
                  // banner 1
                  ScrollWidget(
                    data: database,
                  ),

                  //fitur
                  Padding(
                    padding: const EdgeInsets.only(left: 17),
                    child: Column(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(top: 12),
                          child: Row(children: [
                            const Padding(
                              padding: EdgeInsets.only(
                                left: 38,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                right: 24,
                              ),
                              child: FloatingActionButton(
                                onPressed: () {
                                  print('berfungsi');
                                },
                                child: Image.asset(
                                  'assets/icons/pendakian_icon.png',
                                  width: 21,
                                  height: 21,
                                ),
                                backgroundColor: const Color(0xFFA6E3A7),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                right: 24,
                              ),
                              child: FloatingActionButton(
                                onPressed: () {
                                  print('berfungsi');
                                },
                                child: Image.asset(
                                  'assets/icons/perkiraan_cuaca.png',
                                  width: 38,
                                  height: 38,
                                ),
                                backgroundColor: const Color(0xFFA6E3A7),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                right: 24,
                              ),
                              child: FloatingActionButton(
                                onPressed: () {
                                  print('berfungsi');
                                },
                                child: Image.asset(
                                  'assets/icons/tiket_icon.png',
                                  width: 25,
                                  height: 17.92,
                                ),
                                backgroundColor: const Color(0xFFA6E3A7),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                right: 24,
                              ),
                              child: FloatingActionButton(
                                onPressed: () {
                                  print('berfungsi');
                                },
                                child: Image.asset(
                                  'assets/icons/artikel_icon.png',
                                  width: 22,
                                  height: 22,
                                ),
                                backgroundColor: const Color(0xFFA6E3A7),
                              ),
                            ),
                          ]),
                        ),

                        //text
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0, left: 48),
                          child: Row(
                            children: [
                              Container(
                                  padding:
                                      const EdgeInsets.only(top: 0, right: 16),
                                  child: Text('Pendakian',
                                      style: GoogleFonts.roboto(
                                          fontSize: 8,
                                          fontWeight: FontWeight.w500))),
                              Container(
                                  padding: const EdgeInsets.only(
                                    left: 15,
                                    right: 16,
                                  ),
                                  child: Text('Perkiraan Cuaca',
                                      style: GoogleFonts.roboto(
                                          fontSize: 8,
                                          fontWeight: FontWeight.w500))),
                              Container(
                                  padding: const EdgeInsets.only(
                                    left: 23,
                                    right: 16,
                                  ),
                                  child: Text('Tiket',
                                      style: GoogleFonts.roboto(
                                          fontSize: 8,
                                          fontWeight: FontWeight.w500))),
                              Container(
                                  padding: const EdgeInsets.only(
                                    left: 45,
                                    right: 16,
                                  ),
                                  child: Text('Artikel',
                                      style: GoogleFonts.roboto(
                                          fontSize: 8,
                                          fontWeight: FontWeight.w500)))
                            ],
                          ),
                        )
                      ],
                    ),
                  ),

                  //banner 2
                  ScrollWidget2(
                    data2: database2,
                  ),

                  //Banner 3
                  ScrollWidget3(data3: database3),

                  //Artikel
                  ArticleSection(dataArticle),

                  //banner 4
                  ScrollWidget4(data4: database4)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
