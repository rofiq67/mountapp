class DataBanner4 {
  final String? cityName4;
  final String? provinceName4;
  final String? mountName4;
  final String? note4;
  final double? starPoint4;
  final String? imageUrl4;
  DataBanner4({
    this.cityName4,
    this.provinceName4,
    this.mountName4,
    this.note4,
    this.starPoint4,
    this.imageUrl4,
  });
}
