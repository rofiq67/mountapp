class DataBanner2 {
  final String? cityName2;
  final String? provinceName2;
  final String? mountName2;
  final String? note2;
  final double? starPoint2;
  final String? imageUrl2;
  DataBanner2({
    this.cityName2,
    this.provinceName2,
    this.mountName2,
    this.note2,
    this.starPoint2,
    this.imageUrl2,
  });
}
