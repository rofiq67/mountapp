class DataBanner {
  final String? cityName;
  final String? provinceName;
  final String? mountName;
  final String? note;
  final double? starPoint;
  final String? imageUrl;
  DataBanner({
    this.cityName,
    this.provinceName,
    this.mountName,
    this.note,
    this.starPoint,
    this.imageUrl,
  });
}
