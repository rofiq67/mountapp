class DataBanner3 {
  final String? mountName3;
  final String? note3;
  final String? price3;
  final String? imageUrl3;
  DataBanner3({
    this.mountName3,
    this.note3,
    this.price3,
    this.imageUrl3,
  });
}
