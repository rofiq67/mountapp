import 'package:flutter/material.dart';
import 'package:mountapp/features/info_gunung/presentations/pages/info_gunung.dart';
// import 'package:flutter_native_splash/flutter_native_splash.dart';

//import 'package:google_fonts/google_fonts.dart';

// import 'features/home_page/presentations/pages/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        debugShowCheckedModeBanner: false, home: InfoGunung());
  }
}
